package Array

/*
79. Word Search
link: https://leetcode.com/problems/word-search/
todo：暂时还想不到怎么规避字符重复使用的情况,此题应该是一个深度优先搜索的问题，暂时先去了解深度优先搜索。DFS
 */
func Exist(board [][]byte, word string) bool {
	if len(word) == 0 {
		return true
	}
	if board == nil {
		return false
	}

	r := len(board)
	c := len(board[0])

	horizonIndex := NewStack()
	verticalIndex := NewStack()
	wordIndex := NewStack()
	//-1:不限制 0上  1下  2左  3右
	forbidDirection := NewStack()

	//先找到起点（也即第一个字符）
	for i := range board {
		for j := range board[i] {
			if board[i][j] == word[0] {
				wordIndex.Push(0)
				horizonIndex.Push(i)
				verticalIndex.Push(j)
				forbidDirection.Push(-1)
			}
		}
	}

	//从起点开始在上下左右四个维度去找下一个字符
	for !wordIndex.IsEmpty() {
		currentIndex := wordIndex.Pop().(int)
		nextIndex := currentIndex + 1
		hi := horizonIndex.Pop().(int)
		vi := verticalIndex.Pop().(int)
		fd := forbidDirection.Pop().(int)
		if nextIndex > len(word) - 1 {
			//到这里说明word中所有字符都可以找的到
			return true
		} else {
			//上
			if fd != 0 && hi - 1 >= 0 && word[nextIndex] == board[hi - 1][vi] {
				wordIndex.Push(nextIndex)
				horizonIndex.Push(hi - 1)
				verticalIndex.Push(vi)
				forbidDirection.Push(1)
			}
			//下
			if fd != 1 && hi + 1 <= r - 1 && word[nextIndex] == board[hi + 1][vi] {
				wordIndex.Push(nextIndex)
				horizonIndex.Push(hi + 1)
				verticalIndex.Push(vi)
				forbidDirection.Push(0)
			}
			//左
			if fd != 2 && vi - 1 >= 0 && word[nextIndex] == board[hi][vi - 1] {
				wordIndex.Push(nextIndex)
				horizonIndex.Push(hi)
				verticalIndex.Push(vi - 1)
				forbidDirection.Push(3)
			}
			//右
			if fd != 3 && vi + 1 <= c - 1 && word[nextIndex] == board[hi][vi + 1] {
				wordIndex.Push(nextIndex)
				horizonIndex.Push(hi)
				verticalIndex.Push(vi + 1)
				forbidDirection.Push(2)
			}
		}
	}
	return false
}

/*
79. DFS思想
 */
func exist(board [][]byte, word string) bool {
	b := []byte(word)
	for x := range board {
		for y := range board[x] {
			if DFS(x, y , board, b) {
				return true
			}
		}
	}
	return false
}

//79使用的递归函数, 从某一个点开始的深度搜索, 并且有方向的概念
//我上面的写法的思路是和这个吻合的，自己尝试使用栈来实现迭代类型的实现，结果卡在了如何标记已访问节点上
func DFS(x, y int, board [][]byte ,word []byte) bool {
	if x < 0 || x > len(board) - 1 {
		return false
	}

	if y < 0 || y > len(board[x]) - 1 {
		return false
	}

	if board[x][y] != word[0] {
		return false
	}

	if len(word) == 1 {
		//表明某一条深度递归的搜索路径覆盖到了word字符串中的每一个字符
		return true
	}

	//标记为已访问
	tmp := board[x][y]
	board[x][y] = '-'
	//找到正确的下一个字符，以此字符所在位置继续按照前后左右四个方向进行搜索
	//"||"的特性减少了不必要的计算，只需要找到一条路径即可
	if  DFS(x, y + 1, board, word[1:]) ||
		DFS(x, y - 1, board, word[1:]) ||
		DFS(x - 1, y, board, word[1:]) ||
		DFS(x + 1, y, board, word[1:]) {
		return true
	}
	//没找到就将标记取消
	board[x][y] = tmp
	return false
}

/*
3. Longest Substring Without Repeating Characters
link: https://leetcode.com/problems/longest-substring-without-repeating-characters/
Not as simple as it looks
 */
func LengthOfLongestSubstring(s string) int {
	sa := []byte(s)
	sm := map[byte]int{}
	ml := 0
	//a sliding window
	for i, j := 0 ,0; j < len(sa); j++ {
		if v, ok := sm[sa[j]]; ok { //sa[j] duplicated
		    //easy to make mistakes
		    i = Max(v + 1, i)
		}
		ml = Max(j-i+1, ml)
		sm[sa[j]] = j
	}
	return ml
}

func Max(i, j int) int {
	if i > j {
		return i
	} else {
		return j
	}
}