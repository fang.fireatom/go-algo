package Array

import (
	"fmt"
	"testing"
)

func TestIsOneBitCharacter1(t *testing.T) {
	a1 := []int{1,0,1,0,1,0}
	fmt.Println(isOneBitCharacter(a1))
}

func TestAddToArrayForm(t *testing.T) {
	a := []int{9,9}
	k := 1
	fmt.Println(AddToArrayForm(a, k))
	fmt.Println(AddToArrayFormReform(a, k))
}

func TestArrayPairSum(t *testing.T) {
	a := []int{2,3,4,5,1,9,3,2}
	fmt.Println(ArrayPairSum(a))
	QuickSort(a)
	fmt.Println(a)
}

func TestMaxProfit(t *testing.T) {
	s := []int{3,2,6,5,0,3}
	fmt.Println(MaxProfit(s))
}

func TestMaxProfitTotal(t *testing.T) {
	s := []int{7,1,5,3,6,4}
	fmt.Println(MaxProfitTotal(s))
}

func TestContainsDuplicateHash(t *testing.T) {
	nums := []int{0}
	fmt.Println(ContainsDuplicateHash(nums))
}

func TestPrefixesDivBy5(t *testing.T) {
	a := []int{0,1,1}
	fmt.Println(PrefixesDivBy5(a))
}

func TestFindShortestSubArray(t *testing.T) {
	m := []int{1,2,2,3,1}
	fmt.Println(FindShortestSubArray(m))
}

func TestDuplicateZeros(t *testing.T) {
	arr := []int{1,0,2,3,0,4,5,0}
	duplicateZeros(arr)
	fmt.Println(arr)
}

func TestFairCandySwap(t *testing.T) {
	A := []int{2}
	B := []int{1,3}
	fmt.Println(FairCandySwap(A, B))
}