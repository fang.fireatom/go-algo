package Array

/*
717. 1-bit and 2-bit Characters
link: https://leetcode.com/problems/1-bit-and-2-bit-characters/
 */
func isOneBitCharacter(bits []int) bool {
	//默认输入数组都是合法数组
	i := 0
	l := len(bits)
	for i < l {
		if bits[i] == 1 {
			i = i + 2
		} else { //0
			if i == l - 1 {
				return true;
			} else {
				i++
			}
		}
	}
	return false
}

/*
989. Add to Array-Form of Integer
link: https://leetcode.com/problems/add-to-array-form-of-integer/
 */
//这么写是不会对的，这样肯定会有溢出
//var nat []int
//var na []int
//l := len(A)
//va := 0
//in := 1
//for i := l - 1; i >= 0; i-- {
//	va += A[i] * in
//	in *= 10
//}
//r := va + K
//if r == 0 {
//	nat = append(nat, 0)
//}
//for r != 0 {
//	nat = append(nat, r % 10)
//	r = r / 10
//}
//for i := len(nat) - 1; i >= 0; i-- {
//	na = append(na, nat[i])
//}
//return na
func AddToArrayForm(A []int, K int) []int {
	var kn []int
	var l int
	var ms []int
	var result []int
	v := K
	//do while循环
	for {
		kn = append(kn, v % 10)
		v = v / 10
		if v == 0 {
			break
		}
	}
	//倒置k的位置元素切片
	for i, j := 0, len(A) - 1; i < j; i, j = i + 1, j - 1 {
		A[i], A[j] = A[j], A[i]
	}

	//遍历相加
	la := len(A)
	lk := len(kn)
	if la > lk {
		l = lk
		ms = A
	} else {
		l = la
		ms = kn
	}
	//进位
	c := 0
	r := 0
	for i := 0; i < l; i++ {
		r = A[i] + kn[i] + c
		result = append(result, r % 10)
		c = r / 10
	}

	//进位的单独处理
	lms := len(ms)
	n := l
	nr := 0
	for c != 0 {
		if n < lms {
			nr = ms[n] + c
			result = append(result, nr % 10)
			c = nr / 10
		} else {
			result = append(result, c)
			break
		}
		n++
	}
	for ; n < lms; n++ {
		result = append(result, ms[n])
	}

	//倒置数组
	for i, j := 0, len(result) - 1; i < j; i, j = i +1, j - 1 {
		result[i], result[j] = result[j], result[i]
	}
	return result
}

/*
989 优化版本 [1, 2, 3+912] --> [1, 2+91, 5], 我的主要是上来就把k给拆开成数组了，导致循环时必须先考虑清楚哪个数组更长。这里就把k一点点拆开
循环条件可以写的更简洁
不用考虑溢出是因为已经限制了k的范围
todo 这个循环的逻辑还是要再多想想，想想。
 */
func AddToArrayFormReform(A []int, K int) []int {
	ln := len(A)
	i := ln - 1
	var ra []int
	//进位
	c := 0
	s := 0
	//循环条件写到一起有助于简化代码逻辑
	for i >= 0 || K > 0 || c > 0 {
		//进位是一定要加上的
		s = c
		if i >= 0 {
			s += A[i]
		}

		if K > 0 {
			s += K % 10
		}
		ra = append(ra, s % 10)
		c = s / 10
		K /= 10
		i--
	}

	//倒置数组
	for i, j := 0, len(ra) - 1; i < j; i, j = i + 1, j - 1 {
		ra[i], ra[j] = ra[j], ra[i]
	}

	return ra
}

/*
561. Array Partition I
link: https://leetcode.com/problems/array-partition-i/
 */
func ArrayPairSum(nums []int) int {
	//sort.Ints(nums)
	//用我自己写的快速排序来实现一下, 实际没必要，go里面的排序
	QuickSort(nums)
	ln := len(nums)
	r := 0
	for i := 0; i < ln; i = i + 2 {
		r += nums[i]
	}
	return r
}

func QuickSort(a []int) {
	l := len(a)
	if l < 2 {
		return
	}
	QuickSortRecursion(a, 0, l - 1)
}

func QuickSortRecursion(a []int, p, r int) {
	if p >= r {
		return
	}
	q := Partition(a, p, r)
	QuickSortRecursion(a, p, q - 1)
	QuickSortRecursion(a, q + 1, r)
}

func Partition(a []int, p, r int) int {
	//双指针来实现原地排序,并且涉及交换，是不稳定的排序，也即相同元素的位置会改变
	pivot := a[r]
	i := p
	j := p
	for ; j < r; j++ {
		if a[j] < pivot {
			a[i], a[j] = a[j], a[i]
			i++
		}
	}
	a[i], a[r] = a[r], a[i]
	return i
}

/*
999. Available Captures for Rook
link：https://leetcode.com/problems/available-captures-for-rook/
 */
func NumRookCaptures(board [][]byte) int {
	l := 0
	c := 0
	for i := range board {
		for j := range board[i] {
			if board[i][j] == 'R' {
				l = i
				c = j
				break
			}
		}
	}
	//rook在l行c列处
	np := 0
	for n := l; n >= 0; n-- {
		if board[n][c] == 'B' {
			break
		}
		if board[n][c] == 'p' {
			np++
			break
		}
	}

	for n := l; n < 8; n++ {
		if board[n][c] == 'B' {
			break
		}
		if board[n][c] == 'p' {
			np++
			break
		}
	}

	for n := c; n >= 0; n-- {
		if board[l][n] == 'B' {
			break
		}
		if board[l][n] == 'p' {
			np++
			break
		}
	}

	for n := c; n < 8; n++ {
		if board[l][n] == 'B' {
			break
		}
		if board[l][n] == 'p' {
			np++
			break
		}
	}

	return np
}

/*
999 代码可以更简洁一些，引入数组来模拟方向的概念，代码形式上确有简化
 */
func NumRookCapturesSimplify(board [][]byte) int {
	//使用两个数组来模拟上下左右的移动
	var dx = []int{0, 0, -1, 1}
	var dy = []int{-1, 1, 0, 0}
	np := 0
	l, c := FindPositionR(board)
	for k := 0; k < 4; k++ {
		x, y := l + dx[k], c + dy[k]
		//简化了循环的条件
		for x >= 0 && x < 8 && y >= 0 && y < 8 {
			if board[x][y] == 'B' {
				break
			}
			if board[x][y] == 'p' {
				np++
				break
			}
			x += dx[k]
			y += dy[k]
		}
	}
	return np
}

//找到R所在的位置
func FindPositionR(board [][]byte) (int, int) {
	for i := 0; i < 8; i++ {
		for j := 0; j < 8; j++ {
			if board[i][j] == 'R' {
				return i, j
			}
		}
	}
	return 0, 0
}

/*
121. Best Time to Buy and Sell Stock
link：https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
 */
func MaxProfit(prices []int) int {
	if len(prices) == 0 || len(prices) == 1 {
		return 0
	}
	b := 0
	max := 0

	for i := 1; i < len(prices); i++ {
		if prices[i] > prices[b] {
			mt := prices[i] - prices[b]
			if mt > max {
				max = mt
			}
		} else {
			b = i
		}
	}
	return max
}

/*
122. Best Time to Buy and Sell Stock II
link: https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/
几点收获：
1. 我这里的规律是看数字结合调试才完善的逻辑。缺乏一个清晰的逻辑在里面。看了solution发现实际上我这个的原理是波峰和波谷的概念，可以通过画图来理清逻辑
2. 可以当天卖，然后再买入的，这样的逻辑更简单清晰
 */
func MaxProfitTotal(prices []int) int {
	if len(prices) == 0 || len(prices) == 1 {
		return 0
	}
	//主要是设置了-1为为买入状态
	b := -1
	s := -1
	maxTotal := 0
	//为了末尾的特殊边界条件添加一个哨兵
	prices = append(prices, -1)
	for i := 0; i < len(prices) - 1; i++ {
		if prices[i] < prices[i+1] {
			//没有的时候才买入
			if b == -1 {
				b = i
			}
		} else {
			s = i
			//还没有买入就不存在卖出
			if b == -1 {
				continue
			}
			maxTotal += prices[s] - prices[b]
			//卖出后将状态置成未买入
			b = -1
		}
	}
	return maxTotal
}

/*
122 波峰波谷的实现方法
 */
func MaxProfitTotalWave(prices []int) int {
	if len(prices) == 0 || len(prices) == 1 {
		return 0
	}

	i := 0
	valley := prices[0]
	peak := prices[0]
	maxTotal := 0

	for i < len(prices) - 1 {
		for i < len(prices) - 1 && prices[i] >= prices[i+1] {
			i++
		}
		valley = i
		for i < len(prices) - 1 && prices[i] <= prices[i+1] {
			i++
		}
		peak = i
		maxTotal += prices[peak] - prices[valley]
	}
	return maxTotal
}

/*
122 当天卖出然后当天可以再买入，隔天卖出
 */
func MaxProfitTotalSimple(prices []int) int {
	maxTotal := 0
	for i := 0; i < len(prices); i++ {
		if prices[i] < prices[i+1] {
			maxTotal += prices[i+1] - prices[i]
		}
	}
	return maxTotal
}

/*
217. Contains Duplicate
link: https://leetcode.com/problems/contains-duplicate/
这个不好，因为最差时间复杂度到了n2，如果使用快排，最差是n2，但是很大概率是nlogn，并且语言自带的排序算法会根据数据规模选择不同的排序实现方式，比如合并排序等。
或者使用hash，利用hash的对应关系。
 */
func containsDuplicate(nums []int) bool {
	for i := 0; i < len(nums) - 1; i++ {
		v := nums[i]
		for j := i + 1; j < len(nums); j++ {
			if nums[j] == v {
				return true
			}
		}
	}
	return false
}

/*
122. Hash实现
 */
func ContainsDuplicateHash(nums []int) bool {
	m := make(map[int]int)
	for i, v := range nums {
		_, ok := m[v]; if !ok {
			m[v] = i
			//这里逻辑非常容易出错
			continue
		}
		return true
	}
	return false
}

/*
219. Contains Duplicate II
link: https://leetcode.com/problems/contains-duplicate-ii/
 */
func containsNearbyDuplicate(nums []int, k int) bool {
	var interval int
	m := make(map[int]int)
	for i, v := range nums {
		index, ok := m[v]; if ok {
			interval = i - index
			if interval <= k {
				return true
			}
		}
		m[v] = i
	}
	return false
}

/*
1018. Binary Prefix Divisible By 5
link: https://leetcode.com/problems/binary-prefix-divisible-by-5/
O(n2),思路很容易想，但是这个运行效率太低
 */
func PrefixesDivBy5(A []int) []bool {
	var result []bool
	var sc int
	for i := 0; i < len(A); i++ {
		//除了最低位特殊外，其他的都是以4为轮回变换
		unit := A[i]
		for j := i - 1; j >= 0; j-- {
			if A[j] == 0 {
				continue
			}
			sc = (i - j - 1) % 4
			switch sc {
			case 0:
				unit = (unit + 2) % 10
			case 1:
				unit = (unit + 4) % 10
			case 2:
				unit = (unit + 8) % 10
			case 3:
				unit = (unit + 6) % 10
			}
		}
		if unit == 0 || unit == 5 {
			result = append(result, true)
		} else {
			result = append(result, false)
		}
	}
	return result
}

/*
1018. 使用左移的思路去做
 */
func prefixesDivBy5Left(A []int) []bool {
	res := 0
	ba := make([]bool, len(A))
	for i, v := range A {
		res = (2 * res + v) % 5
		if res == 0 {
			ba[i] = true
		} else {
			ba[i] = false
		}
	}
	return ba
}

/*
605. Can Place Flowers
link: https://leetcode.com/problems/can-place-flowers/
思路是对的，主要是特殊情况下考虑不清楚，边界条件被弄的很复杂，第一个和最后一个位置的特殊处理不优雅
所有都遍历才给出结果，实际上更好的是如果满足条件的值达到n就应该返回结果并退出
 */
func canPlaceFlowers(flowerbed []int, n int) bool {
	satisfy := false
	i := 0
	if len(flowerbed) == 1 {
		if flowerbed[0] == 0 {
			flowerbed[0] = 1
			n--
		}
	} else {
		for i < len(flowerbed) {
			if i == 0 {
				if flowerbed[i] == 0 && flowerbed[i+1] == 0 {
					flowerbed[i] = 1
					n--
				}
				i += 1
			} else if i == len(flowerbed) - 1 {
				if flowerbed[i-1] == 0 && flowerbed[i] == 0 {
					flowerbed[i] = 1
					n--
				}
				i += 1
			} else {
				if flowerbed[i-1] == 0 && flowerbed[i] == 0 && flowerbed[i+1] == 0 {
					flowerbed[i] = 1
					n--
				}
				//有些情况是可以往前移动2位，但是这里没必要区分的那么细增加复杂度
				i += 1
			}
		}
	}
	if n <= 0 {
		satisfy = true
	}
	return satisfy
}

/*
605：更优雅的循环条件写法
 */
func canPlaceFlowersElegant(flowerbed []int, n int) bool {
	count := 0
	i := 0
	for i < len(flowerbed) {
		if flowerbed[i] == 0 && (i == 0 || flowerbed[i-1] == 0) && (i == len(flowerbed) - 1 || flowerbed[i+1] == 0) {
			flowerbed[i] = 1
			count++
		}
		if count >= n {
			return true
		}
		i++
	}
	return false
}

/*
697. Degree of an Array
link: https://leetcode.com/problems/degree-of-an-array/
 */
func FindShortestSubArray(nums []int) int {
	s := 1
	t := 0
	minIndex := make(map[int]int)
	maxIndex := make(map[int]int)
	freMap := make((map[int]int))

	for k, v := range nums {
		if _, ok := minIndex[v]; ok {
			maxIndex[v] = k
			freMap[v]++
		} else {
			minIndex[v] = k
			freMap[v] = 1
		}
	}

	mn := largestMap(freMap)
	si := getIndex(mn, freMap)

	if mn == 1 {
		s = 1
	} else {
		s = maxIndex[si[0]] - minIndex[si[0]] + 1
		for _, v := range si {
			t = maxIndex[v] - minIndex[v] + 1
			if s > t {
				s = t
			}
		}
	}
	return s
}

/*
697: 返回map中最大值
 */
func largestMap(freMap map[int]int) int {
	mn := 0
	for i := range freMap {
		if mn < freMap[i] {
			mn = freMap[i]
		}
	}
	return mn
}

/*
697: 获取最大次数对应的索引
 */
func getIndex(mn int, freMap map[int]int) []int {
	var si []int
	for i := range freMap {
		if freMap[i] == mn {
			si = append(si, i)
		}
	}
	return si
}

/*
1089. Duplicate Zeros
link: https://leetcode.com/problems/duplicate-zeros/
直接arr = tmp是不行的，需要指针。但是这种单个元素赋值就可以
 */
func DuplicateZeros(arr []int)  {
	tmp := make([]int, len(arr))
	i := 0
	j := 0

	for j < len(arr) {
		if arr[i] == 0 {
			tmp[j] = 0
			j += 1
			if j < len(arr) {
				tmp[j] = 0
				j += 1
			} else {
				break
			}
			i += 1
		} else {
			tmp[j] = arr[i]
			j += 1
			i += 1
		}
	}

	for i := 0; i < len(arr); i++ {
		arr[i] = tmp[i]
	}
}

/*
1089。Duplicate Zeros
原地操作,最容易想到的思路。性能不佳，待优化。
 */
func duplicateZeros(arr []int) {
	i := 0
	for i < len(arr) {
		if arr[i] == 0 {
			for j := len(arr) - 1; j > i + 1; j-- {
				arr[j] = arr[j - 1]
			}
			if i < len(arr) - 1 {
				arr[i + 1] = 0
			}
			i += 2
		} else {
			i++
		}
	}
}

/*
888. Fair Candy Swap
link: https://leetcode.com/problems/fair-candy-swap/
o(n2)时间复杂度，效率相当低。
 */
func FairCandySwap(A []int, B []int) []int {
	ans := make([]int,2)
	s := subArray(A, B)
	bMap := make(map[int]int)
	for i, v := range B {
		bMap[v] = i
	}

	for i := 0; i < len(A); i++ {
		bv := A[i] - s / 2
		if _, ok := bMap[bv]; ok {
			ans[0] = A[i]
			ans[1] = bv
			break
		}
	}
	//这个效率很低，应该引入hash表的形式来做
	//for i := 0; i < len(A); i++ {
	//	for j := 0; j < len(B); j++ {
	//		if A[i] - B[j] == s / 2 {
	//			ans[0] = A[i]
	//			ans[1] = B[j]
	//		}
	//	}
	//}
	return ans
}

/*
888. 计算数组的加和的差
goal：为了防止加和溢出
 */
func subArray(a, b []int) int {
	s := 0
	for i := 0; i < len(b) && i < len(a); i++ {
		s += a[i] - b[i]
	}

	for j := len(b); j < len(a); j++ {
		s += a[j]
	}

	//这里需要注意函数计算的是a - b
	for j := len(a); j < len(b); j++ {
		s -= b[j]
	}

	return s
}
