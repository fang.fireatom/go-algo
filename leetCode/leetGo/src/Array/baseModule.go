package Array

//简单的栈结构，不限制容量，使用go内部提供的扩容机制
type stack struct {
	data []interface{}
	top int
}

func (this *stack) IsEmpty() bool {
	if this.top < 0 {
		return true
	}
	return false
}

//栈的这个容量问题写错好几次了
func (this *stack) Push(v interface{}) {
	this.top++
	if this.top > len(this.data) - 1 {
		this.data = append(this.data, v)
	} else {
		this.data[this.top] = v
	}
}

func (this *stack) Pop() interface{} {
	if this.IsEmpty() {
		return nil
	}
	v := this.data[this.top]
	this.top--
	return v
}

//默认给栈100的容量
func NewStack() *stack {
	return &stack{
		make([]interface{}, 0, 32),
		-1,
	}
}