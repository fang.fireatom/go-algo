package Array

import (
	"fmt"
	"testing"
)

func TestExist(t *testing.T) {
	//board := [][]byte {
	//	{'A','A','A','A'},
	//	{'A','A','A','A'},
	//	{'A','A','A','A'},
	//}
	board := [][]byte {
		{'A','A'},
		{'A','A'},
	}
	word := "AAAAA"
	fmt.Println(exist(board, word))
}

func TestLengthOfLongestSubstring(t *testing.T) {
	s := "abba"
	fmt.Println(LengthOfLongestSubstring(s))
}
