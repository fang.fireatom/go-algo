package LinkedList

/*
237. Delete Node in a Linked List
link: https://leetcode.com/problems/delete-node-in-a-linked-list/
主要目的是在不知道要删除节点的前序节点情况下删除当前节点:相当于删除节点变成next节点，然后该节点指向原始的next的next节点
 */
/**
* Definition for singly-linked list.
* type ListNode struct {
*     Val int
*     Next *ListNode
* }
*/
//func deleteNode(node *ListNode) {
//	node.Val = node.Next.Val
//	node.Next = node.Next.Next
//}

/*
160. Intersection of Two Linked Lists
link：https://leetcode.com/problems/intersection-of-two-linked-lists/
时间复杂度O(N)，空间复杂度O(1),有了这个限制，这题才有意思
 */
 type ListNode struct {
     Val int
     Next *ListNode
 }

 /*
 使用hash表来存放节点地址，虽说空间复杂度变高了，但是思路不失为一个很好的思路
 todo leetcode校验我这个出错了，理论上讲是不应该的。猜测是测试用例有点问题
  */
func getIntersectionNodeHash(headA, headB *ListNode) *ListNode {
	aMap := make(map[*ListNode]int)
	bMap := make(map[*ListNode]int)

	cA := headA
	cB := headB

	for cA != nil {
		aMap[cA] = 1
		cA = cA.Next
	}
	for cB != nil {
		bMap[cB] = 1
		cB = cB.Next
	}

	for k := range aMap {
		if _, ok := bMap[k]; ok {
			return k
		}
	}
	return nil
}

/*
使用双指针可以在o(1)的空间复杂度下找到相交点
遍历，相互交换指针所在位置.一轮交换之后一定会相遇（也即当前节点指针相等），有交点时相等处的值不为nil，没有时为nil
 */
func getIntersectionNode(headA, headB *ListNode) *ListNode {
	cA := headA
	cB := headB

	for cA != nil || cB != nil {
		if cA == cB {
			return cA
		}

		if cA == nil {
			cA = headB
			cB = cB.Next
			continue
		}

		if cB == nil {
			cB = headA
			cA = cA.Next
			continue
		}

		cA = cA.Next
		cB = cB.Next
	}

	return nil
}

