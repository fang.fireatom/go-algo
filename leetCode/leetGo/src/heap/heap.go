package heap

import "container/heap"

/*
703. Kth Largest Element in a Stream
link: https://leetcode.com/problems/kth-largest-element-in-a-stream/
用堆排序的思路来做...好像一直理解错题意了。
我这个解决的问题是一个数组每次只加一个元素后返回第k大数字,这个就是练习了堆排序
 */
//type KthLargest struct {
//	nums []int
//	k    int
//}
//
//func Constructor(k int, nums []int) KthLargest {
//	return KthLargest{
//		nums: nums,
//		k: k,
//	}
//}
//
//func (this *KthLargest) Add(val int) int {
//	nums := this.nums
//	nums = append(nums, val)
//	BuildHeap(nums)
//
//	return GetKthLargest(nums, this.k)
//}
//
//func BuildHeap(nums []int) {
//	c := len(nums)
//	if c <= 1 {
//		return
//	}
//	k := 0
//	if (c-1) % 2 == 0 {
//		k = (c-1) / 2 -1
//	} else {
//		k = (c-1) / 2
//	}
//	for j := k; j >= 0; j-- {
//		heapify(nums, c, j)
//	}
//}
//
//func GetKthLargest(nums []int, k int) int {
//	c := len(nums)
//	for i := 0; i < k; i++ {
//		nums[0], nums[c-i-1] = nums[c-i-1], nums[0]
//		heapify(nums, c-i-1, 0)
//	}
//	return nums[c-k]
//}
//
//func heapify(nums []int, c, k int) {
//	i := k
//	for {
//		heapMax := i
//		if 2*i+1 < c && nums[i] < nums[2*i+1] {
//			heapMax = 2*i+1
//		}
//		if 2*i+2 < c && nums[heapMax] < nums[2*i+2] {
//			heapMax = 2*i+2
//		}
//		if heapMax == i {
//			break
//		}
//		nums[i], nums[heapMax] = nums[heapMax], nums[i]
//		i = heapMax
//	}
//}

/*
703. Kth Largest Element in a Stream
排序的思路来做
实话说实现的真的不好，时间复杂度很高，对特殊情况的支持很不好。比如length = k-1时，必须要经过一次全排序才能得到正确结果
并且整个解法核心思想都是排序，无非定制化了排序，借用了插入排序的思想。别人来看很不好理解。我也是不断提交才发现有些特殊情况不满足
 */
//type KthLargest struct {
//	nums []int
//	k    int
//}
//
//func Constructor(k int, nums []int) KthLargest {
//	sort.Ints(nums)
//	return KthLargest{
//		nums: nums,
//		k: k,
//	}
//}
//
//func (this *KthLargest) Add(val int) int {
//	n := len(this.nums)
//	k := this.k
//	if n < k {
//		this.nums = append(this.nums, val)
//		sort.Ints(this.nums)
//		return this.nums[len(this.nums)-k]
//	}
//	if val <= this.nums[n-k] {
//		//小于前k位数字的对结果无影响，不存储，直接返回结果
//		return this.nums[n-k]
//	}
//	this.nums = append(this.nums, val)
//	for i := n - 1; i >= n - k; i-- {
//		//做插入排序
//		if this.nums[i] > val {
//			this.nums[i+1] = this.nums[i]
//		} else {
//			this.nums[i+1] = val
//			break
//		}
//	}
//	return this.nums[len(this.nums)-k]
//}

/*
703. Kth Largest Element in a Stream
link: https://leetcode.com/problems/kth-largest-element-in-a-stream/
经过一番折腾，发现还是思路实在死板了，换成小顶堆，这堆顶肯定是最小的，假设堆有k个元素，那就可以说堆顶是第k大
虽然通过了，但是这个自己写的堆，方法给的不完善，如果提供push，pop方法，实际上是可以简化操作，我这个还是有太多次的堆化过程了
但是规模是o(k)啊，为何效率这么低？constructor是o(n),那也就只运行了一次啊，为啥效率这么低
插入数据时是使用从上到下的堆化，处理的是一半的节点。这种情况下从下往上处理反而因为只需要处理一个点时间消耗更低
 */
//type KthLargest struct {
//	nums []int
//	k    int
//}
//
//func Constructor(k int, nums []int) KthLargest {
//	//全数组的堆化
//	BuildHeap(nums)
//	for len(nums) > k {
//		nums[0],nums[len(nums)-1] = nums[len(nums)-1], nums[0]
//		nums = nums[:len(nums)-1]
//		heapify(nums, len(nums), 0)
//	}
//	return KthLargest{
//		nums: nums,
//		k: k,
//	}
//}
//
//func (this *KthLargest) Add(val int) int {
//	if len(this.nums) >= this.k && val < this.nums[0] {
//		//跟堆顶相比，小于堆顶无需操作。完全为了看是否运行时间会降低。如果优化效果不明显，这么写反而让代码变的更不好懂了
//		return this.nums[0]
//	}
//	this.nums = append(this.nums, val)
//	//全数组的堆化，跟k的规模有关系
//	BuildHeap(this.nums)
//	if len(this.nums) > this.k {
//		this.nums[0], this.nums[len(this.nums)-1] = this.nums[len(this.nums)-1], this.nums[0]
//		this.nums = this.nums[:len(this.nums)-1]
//		heapify(this.nums, len(this.nums), 0)
//	}
//	return this.nums[0]
//}
//
//func BuildHeap(nums []int) {
//	c := len(nums)
//	if c <= 1 {
//		return
//	}
//	k := 0
//	if (c-1)%2 == 0 {
//		k = (c-1)/2-1
//	} else {
//		k = (c-1)/2
//	}
//	for j := k; j >= 0; j-- {
//		heapify(nums, c, j)
//	}
//}
//
//func heapify(nums []int, c, k int) {
//	i := k
//	for {
//		heapMin := i
//		if 2*i+1 < c && nums[i] > nums[2*i+1] {
//			heapMin = 2*i+1
//		}
//		if 2*i+2 < c && nums[heapMin] > nums[2*i+2] {
//			heapMin = 2*i+2
//		}
//		if heapMin == i {
//			break
//		}
//		nums[i], nums[heapMin] = nums[heapMin], nums[i]
//		i = heapMin
//	}
//}

/*
703. 使用小顶堆，调用go提供的heap接口实现
 */
// An IntHeap is a min-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type KthLargest struct {
	nums *IntHeap
	k    int
}

func Constructor(k int, nums []int) KthLargest {
	h := &IntHeap{}
	heap.Init(h)
	//for i := 0; i < len(nums); i++ {
	//	heap.Push(h, nums[i])
	//}

	//开始写成了v := range nums,这时的v实际上是索引
	for _, v := range nums {
		heap.Push(h, v)
	}

	for h.Len() > k {
		//循环堆中元素只剩k个时，堆顶就是第k大数字
		heap.Pop(h)
	}

	return KthLargest{h, k}
}

func (this *KthLargest) Add(val int) int {
	if this.nums.Len() < this.k {
		heap.Push(this.nums, val)
	} else if (*this.nums)[0] < val {
		//新增的数值小于堆顶时才会对结果产生影响
		heap.Push(this.nums, val)
		heap.Pop(this.nums)
	}
	return (*this.nums)[0]
}

/*
1046. Last Stone Weight
link: https://leetcode.com/problems/last-stone-weight/
The core is to achieve a big top heap, the easiest way is to directly change the up and down functions of go's heap package to generate a new package.
Here, for exercise, write a large top heap to achieve following the heap package (the core is to achieve the up and down functions)
 */
func swap(h []int, i, j int) {
	h[i], h[j] = h[j], h[i]
}

func lagre(i, j int) bool {
	return i > j
}

func up(h []int, i int) {
	for {
		j := (i - 1) / 2 //parent
		//j == i cover the boundary condition i = 0
		if j == i || !lagre(h[i], h[j]) {
			break
		}
		swap(h, i, j)
		i = j
	}
}

func down(h []int, i0, n int) bool {
	i := i0
	for {
		 j1 := 2*i + 1
		 if j1 >= n || j1 < 0 { // j1 < 0 after int overflow
		 	break
		 }
		 j := j1
		 if j2 := j1 + 1; j2 < n && lagre(h[j2], h[j1]) { //right node
		 	j = j2
		 }
		 if !lagre(h[j], h[i]) {
		 	break
		 }
		 swap(h, i, j)
		 i = j
	}
	return i > i0
}

func heapify(h []int, n int)  {
	for i := n/2 - 1; i >= 0; i-- { //pay attention to the relationship between parent and son index
		down(h, i, n)
	}
}

func pop(h *[]int) int {
	// -1 means the slice is empty
	if len(*h) == 0 {
		return -1
	}
	old := *h
	n := len(old)
	swap(old, 0, n-1)
	down(old, 0, n-1)

	v := old[n-1]
	*h = old[:n-1]
	return v

}

func push(h *[]int, v int) {
	*h = append(*h, v)
	up(*h, len(*h)-1)
}

func lastStoneWeight(stones []int) int {
	heapify(stones, len(stones))
	for {
		//i love this
		m := pop(&stones)
		l := pop(&stones)
		if m == -1 {
			return 0
		}
		if l == -1 {
			return m
		}
		if m - l == 0 {
			continue
		} else {
			push(&stones, m-l)
		}
	}
}

/*
这个的逻辑实现可以说很优美了，不像我那个逻辑上有点绕。
func lastStoneWeight(stones []int) int {
	pq := PQ(stones)
	heap.Init(&pq)
	for len(pq) >= 2 {
		a, b := heap.Pop(&pq).(int), heap.Pop(&pq).(int)
		if a == b {
			continue
		}
		heap.Push(&pq, a-b)
	}
	if len(pq) == 0 {
		return 0
	}
	return pq[0]
}
 */
