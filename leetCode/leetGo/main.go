package main

import (
	"fmt"
	"regexp"
)

/*
测试一些使用方法
 */
func main() {
	//保留散列表中最大的值,重复的无法删除，这么写有问题
	//m := make(map[int]int)
	//m[10] = 9
	//for i := 0; i < 10; i++ {
	//	m[i] = i
	//}
	//mk := 0
	//for j := range m {
	//	if m[j] > m[mk] {
	//		delete(m, mk)
	//		mk = j
	//	} else if m[j] < m[mk] {
	//		delete(m, j)
	//	}
	//}
	//fmt.Println(m)

	reg := regexp.MustCompile(`cn=([^,\)]*)`)
	res := reg.FindStringSubmatch("(&(objectClass=inetOrgPerson)(cn=fang.yuan))")
	fmt.Println(res[1])
	fmt.Println(-1/2)
	a := []int{1}
	add(a)
	fmt.Println(a[0])

	names := make([]int, 4)
	names[0] = 2
	fmt.Println(names[1], names[0])
}

func add(arr []int) {
	arr[0]++
}
