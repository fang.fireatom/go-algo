package _7_skiplist

import (
	"math/rand"
)

/*
写这种数据结构的思路：
  1.把最基本的数据组织结构写出来
  2.实现此种数据结构要求的特性：通过依附的方法来实现
  todo 总感觉一个索引节点里面的forwards数组占用的空间很大。含多个变量在里面。
  每一层forwards数组里存放的是的node都是一样的。这样来实现一个寻址的功能。
  forwards数组中存放指针而不是原node变量是为了节省内存空间，这样理解空间复杂度确实是n
 */

//跳表数据结构
//设置头节点和当前索引的最高层数，最小是1层
//链表的结构主要是通过一个个相互关联的node节点来实现，skiplist这个结构体更像是一个组织形式，里面的参数可以依据需要进行设定
//为了查找方便，索引的最高层数是需要定义到基本属性中去.head代表查找起始节点，所以也需要定义到属性中去
type SkipList struct {
	levelCount int
	head       *SkipListNode
}

//跳表中节点数据结构
//接受的value值不限制，但是这里做了简化，为了便于比较，还是默认正常插入的数值是正数
type SkipListNode struct {
	value     int
	forwards []*SkipListNode
	maxLevel int
}

//此跳表的最大索引层数
const MAX_LEVEL  = 16

//实际起作用的是为头节点设置的forwards数组长度（深度）
func NewSkipListNode(v, maxLevel int) *SkipListNode {
	return &SkipListNode{v, make([]*SkipListNode, MAX_LEVEL), maxLevel}
}

func NewSkipList() *SkipList {
	//设置头节点，简化操作.默认设定的最大层数范围下每一层都有一个头节点(这个参数没啥特别的实际意义，没所谓，不用纠结)
	return &SkipList{1, NewSkipListNode(-1, MAX_LEVEL)}
}

//插入新节点
func (this *SkipList) Insert(v int) {
	level := this.RandomLevel()
	p := this.head
	newNode := NewSkipListNode(v, level)
	update := make([]*SkipListNode, level)
	//寻找索引的更新点
	for i := level - 1; i >= 0; i-- {
		for p.forwards[i] != nil && p.forwards[i].value < v {
			p = p.forwards[i]
		}
		//代表每一层索引应该添加的位置
		update[i] = p
	}
	//在相应层插入相应的索引
	for i := 0; i < level; i++ {
		//这个就很像链表的插入节点了
		newNode.forwards[i] = update[i].forwards[i]
		update[i].forwards[i] = newNode
	}

	//更新跳表的索引层数
	if this.levelCount < level {
		this.levelCount = level
	}
}

//查找数值
func (this *SkipList) Find(v int) *SkipListNode {
	maxLevel := this.levelCount
	p := this.head
	for i := maxLevel - 1; i >= 0; i-- {
		//这里遇到相等直接返回，算不算优化，还是徒增代码复杂度？
		if p.forwards[i] != nil && p.forwards[i].value < v {
			p = p.forwards[i]
		}
	}
	if p.forwards[0] != nil && p.forwards[0].value == v {
		return p.forwards[0]
	} else {
		return nil
	}
}

//根据数值删除节点,-1代表无删除的操作发生，0代表删除成功
//这个是参考示例的删除方法的实现方案
func (this *SkipList) Delete(v int) int {
	deResult := -1
	//每一层可能要更新的节点
	maxLevel := this.levelCount
	//动态的变量值不能作为数组的长度参数，这种动态的使用切片，虽然底层也是数组。
	//错误示范update := [maxLevel]*SkipListNode{} 报错信息：non-constant array bound maxLevel
	update := make([]*SkipListNode, maxLevel)
	p := this.head
	for i := maxLevel - 1; i >= 0; i-- {
		if p.forwards[i] != nil && p.forwards[i].value < v {
			p = p.forwards[i]
		}
		update[i] = p
	}
	//要在最底层找到相应的值才会进行删除操作
	if p.forwards[0] != nil && p.forwards[0].value == v {
		//更新不同层下的索引，直接指向删除节点的才需要更新对应层的forwards数组
		for i := maxLevel - 1; i >= 0; i-- {
			if update[i].forwards[i] != nil && update[i].forwards[i].value == v {
				update[i].forwards[i] = update[i].forwards[i].forwards[i] //我写p.forwards[i]应该也没错
			}
		}
		deResult = 0
	}
	//更新跳表的高度
	if this.levelCount > 1 && p.forwards[this.levelCount-1] == nil {
		this.levelCount--
	}
	return deResult
}

//按照自己的理解实现的删除方法
//todo 写麻烦了，调用find函数竟然只是为了确定节点的索引层数，查找的过程明明就可以记录下各个层次的前序节点，但是这里是先找到需要的节点然后又从每一级的head起始节点开始遍历寻找
func (this *SkipList) DeleteFire(v int) int {
	p := this.head
	s := this.Find(v)
	if s != nil {
		level := s.maxLevel
		for i := level - 1; i >=0; i-- {
			if p.forwards[i] != nil && p.forwards[i].value < v {
				p = p.forwards[i]
			}
			//直接做替换应该就行，因为一个节点不会有越级的索引，索引确定最高层级后就是连续的
			p.forwards[i] = p.forwards[i].forwards[i]
			//if p.forwards[i].value == v {
			//	p.forwards[i] = p.forwards[i].forwards[i]
			//}
		}
		return 0
	} else {
		return -1
	}
}

//跳表插入节点的索引的最大层数
func (this *SkipList) RandomLevel() int {
	var level int = 1
	for i := 0; i < MAX_LEVEL; i++ {
		//严重伪随机？
		if rand.Int31() % 2 == 1 {
			level++
		}
	}
	return level
}
