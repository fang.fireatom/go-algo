package _7_skiplist

import (
	"fmt"
	"testing"
)

//简单的测试了下没有问题
//todo 继续整理这一块的逻辑
func TestNewSkipList(t *testing.T) {
	sl := NewSkipList()
	for i := 1; i < 9; i++ {
		sl.Insert(i)
	}
	fmt.Println(sl.Find(2).maxLevel)
	fmt.Println(sl.DeleteFire(2))
	fmt.Println(sl.Find(3).value)
	fmt.Println(sl.Delete(3))
	fmt.Println(sl.Find(1).forwards[0].value)
}
