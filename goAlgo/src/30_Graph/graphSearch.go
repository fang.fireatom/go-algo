package _0_Graph

import (
	"container/list"
	"fmt"
)

//无向图，无向的概念我认为主要体现在插入关系的时候，无向的需要一次添加进两个元素
type Graph struct {
	adj []*list.List
	v   int
}

func NewGraph(v int) *Graph {
	graph := new(Graph)
	graph.adj = make([]*list.List, v)
	//初始化list，不初始化也能用的样子，还需要继续研究
	for i := range graph.adj {
		graph.adj[i] = list.New()
	}
	graph.v = v
	return graph
}

//无序图要双向插入数据
func (this *Graph) addEdge(s, t int) {
	this.adj[s].PushBack(t)
	this.adj[t].PushBack(s)
}

//广度遍历搜索
func (this *Graph) Bfs(s, t int) {
	if s == t {
		return
	}
	//这样初始化出来的都是false，这样在意义上是对的
	visited := make([]bool, this.v)
	visited[s] = true

	//初始化用来记录路径的pre数组
	pre := make([]int, this.v)
	for i := range pre {
		pre[i] = -1
	}

	//用切片的思想来简单的实现一个队列，存放搜索的点
	var queue []int
	queue = append(queue, s)

	//标识变量表示是否找到变量
	isFound := false

	//循环实现广度优先搜索
	for len(queue) != 0 && !isFound {
		top := queue[0]
		queue = queue[1:]
		tList := this.adj[top]
		//遍历节点对应的链表
		for e := tList.Front(); e != nil; e = e.Next() {
			n := e.Value.(int)
			//节点标识是唯一的，所以可以根据索引判断是否已被访问过
			if !visited[n] {
				pre[n] = top
				if n == t {
					isFound = true
					break
				}
				visited[n] = true
				queue = append(queue, n)
			}
		}
	}
	if isFound {
		PrintPre(pre, s, t)
	} else {
		fmt.Printf("no path found from %d to %d\n", s, t)
	}
}

//这个递归打印还要多理解下
func PrintPre(pre []int, s, t int) {
	//pre[t] = -1的情况如果出现，说明从s到t是没有合法路径的
	if t != s && pre[t] != -1 {
		PrintPre(pre, s, pre[t])
	}
	fmt.Printf("%d ", t)
}

//我理解的深度优先搜索的核心是解决广度优先搜索的层的顺序限制（通过队列的结构来保证的顺序）
//核心是使用了pre这个辅助变量，路径全靠这个记录下来，具体操作过程中，不用考虑打印问题
//todo 还是对递归不是那么理解，这里还是暂时使用递归实现好了
func (this *Graph) Dfs(s, t int) {
	//存储路径
	pre := make([]int, this.v)
	for i := range pre {
		pre[i] = -1
	}

	//记录已经被访问的节点
	visited := make([]bool, this.v)
	visited[s] = true

	//标记是否找到t
	found := false

	this.recurDfs(s, t, pre, visited, found)
	PrintPre(pre, s, t)
}

//todo 这个里面visited是公用的，看似回溯的时候需要重新置false，但是从深度搜索的实现来看是没必要的。这一点还需多理解
//todo 关于深度的理解。一条线往下走直到走不通为止。递归我都理解不太好，迭代更无从谈起（理解透彻后使用迭代实现一次）
func (this *Graph) recurDfs(s, t int, pre []int, visited []bool, found bool) {
	//当某个搜索找到之后，将found置成true后，某个还在执行的搜索应该停止
	//这里和129行加入的found判断有功能重复
	if found {
		return
	}

	visited[s] = true

	//当前节点的深度搜索找到了需要的节点
	if s == t {
		found = true
		return
	}

	sList := this.adj[s]
	//加到这里做一个found判断，就不用到recurDfs里面判断了
	for e := sList.Front(); e != nil && !found; e = e.Next() {
		k := e.Value.(int)
		if !visited[k] {
			pre[k] = s
			//这里把found变量传进去才是对的
			this.recurDfs(k, t, pre, visited, found)
		}
	}

}

