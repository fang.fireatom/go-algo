package _0_Graph

import (
	"testing"
)

func TestGraph_Bfs(t *testing.T) {
	g := NewGraph(8)
	g.addEdge(0, 1)
	g.addEdge(0, 3)
	g.addEdge(1, 2)
	g.addEdge(1, 4)
	g.addEdge(4, 3)
	g.addEdge(4, 5)
	g.addEdge(4, 6)
	g.addEdge(5, 2)
	g.addEdge(5, 7)
	g.addEdge(6, 7)
	//for e := g.adj[5].Front(); e != nil; e = e.Next() {
	//	fmt.Println(e.Value.(int))
	//}
	//g.Bfs(0, 6)
	g.Dfs(0, 6)
}
