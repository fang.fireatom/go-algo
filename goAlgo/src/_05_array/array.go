package _05_array

import (
	"errors"
	"fmt"
)

/**
 * 1）数组设置成一个结构体
 * 2）包含一个切片存放数据，一个uint数存放数组的长度
 * 3）切片是go自己定义的，具有长度和容量两个属性
 */
type Array struct {
	data []int
	length uint
}

//为数组初始化内存
func NewArray(capacity uint) *Array {
	if capacity == 0 {
		return nil
	}
	return &Array{
		data: make([]int, capacity, capacity),
		length: 0,
	}
}

//近似理解为Array类的内置方法
func (this *Array) Len() uint {
	return this.length
}

//判断数组是否越界
func (this *Array) isIndexOutOfRange(index uint) bool {
	//类型限制的很死，如果我不做uint的转换，这个比较就会出错
	if index >= uint(cap(this.data))  {
		return true
	}
	return false
}

//通过索引查询数组
func (this *Array) Find(index uint) (int, error) {
	if this.isIndexOutOfRange(index) {
		return 0, errors.New("out of index range")
	}
	return this.data[index], nil
}

//插入数值到索引index上
func (this *Array) Insert(index uint, v int) error {
	if this.Len() == uint(cap(this.data)) {
		return errors.New("full array")
	}
	//这里的意思是上面的语句保证len是小于容量cap的，所以index==len时肯定可以插入，这样判断的逻辑有点奇怪
	if index != this.Len() && this.isIndexOutOfRange(index) {
		return errors.New("out of index range")
	}
	//插入一个数字，表示数组需要从插入地点向后移动数据
	for i := this.Len(); i > index; i-- {
		this.data[i] = this.data[i-1]
	}
	this.data[index] = v
	this.length++
	return nil
}

//插入数据到尾部
func (this *Array)InsertToTail(v int) error {
	//insert函数的返回值类型和现在这个函数是一致的，所以不报错
	//这个插入的索引也是对的，否则变成替换了
	return this.Insert(this.Len(), v)
}

//删除指定索引的值
func (this *Array) Delete(index uint) (int, error) {
	if this.isIndexOutOfRange(index) {
		return 0, errors.New("out of index range")
	}
	//删除是通过覆盖来实现的，所以删除前要先记录下
	v := this.data[index]
	//画出模型来确定循环的边界
	//最好想的就是删除后i最大也就只能取到len-1
	for i := index; i < this.Len() - 1; i++ {
		this.data[i] = this.data[i+1]
	}
	this.length--
	return v, nil
}

//打印数组
func (this *Array) Print() {
	var format string
	for i := uint(0); i < this.Len(); i++ {
		format += fmt.Sprintf("|%+v", this.data[i])
	}
	fmt.Println(format)
}

//数组实现LRU算法
//换一种思路，最新的数据放在后面,放最前面的话每次插入新的数都要整体做一次搬移
func (this *Array)Lru(v int) (uint, error) {
	for i := uint(0); i < this.Len(); i++ {
		if this.data[i] == v {
			//数据搬移
			for j := i; j < this.Len() - 1; j++ {
				this.data[j] = this.data[j + 1]
			}
			this.data[this.Len() - 1] = v
			return i, nil
		}
	}
	if this.Len() < uint(cap(this.data)) {
		this.data[this.Len()] = v
		this.length++
		return this.Len(), nil
	} else {
		for i := uint(0); i < this.Len() - 1; i++ {
			this.data[i] = this.data[i + 1]
		}
		this.data[this.Len() - 1] = v
		return this.Len() - 1, nil
	}
}

