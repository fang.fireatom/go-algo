package _6_linkeddist

import "fmt"

//不包含哨兵节点的总节点数
func creatCircleList(totalNodeNum, circleNodeNum int) *LinkedList {
	l := NewLinkedList()
	noCircleNum := totalNodeNum - circleNodeNum
	current := l.head

	//初始化含环链表
	tmp := NewListNode(noCircleNum)
	for i := 0; i <= totalNodeNum; i++ {
		if i == noCircleNum {
			current.next = tmp
			current = tmp
			continue
		}

		if i == totalNodeNum {
			current.next = tmp
			continue
		}

		newNode := NewListNode(i)
		current.next = newNode
		current = newNode
	}
	return l
}

func printCircleList(totalNodeNum int, l *LinkedList) {
	current := l.head
	for i := 0; i <= totalNodeNum; i++ {
		fmt.Println(current.next.value)
		current = current.next
	}
}

//可以推导出来当龟兔第一次相遇时，环中相遇点到入口和初始节点到入口的距离相同。（稍微注意下哨兵节点不要计入即可）
func circleCheck(l *LinkedList) *ListNode {
	//龟兔算法,跳过哨兵
	walker := l.head.next
	runner := l.head.next

	for runner != nil && runner.next != nil {
		walker = walker.next
		runner = runner.next.next
		if walker == runner {
			break
		}
	}

	if walker != runner {
		return nil
	}

	walker = l.head.next

	for runner != walker {
		walker = walker.next
		runner = runner.next
	}

	return runner
}