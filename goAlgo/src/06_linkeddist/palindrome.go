package _6_linkeddist

import "fmt"

func isPalindrome(l *LinkedList) bool {
	lLen := l.length
	//临界的条件必须要考虑
	if lLen == 0 {
		return false
	}
	if lLen == 1 {
		//只有一个节点当然是回文链表
		return true
	}
	var isPalindrome bool = true
	step := lLen/2
	var next *ListNode = nil
	//去除哨兵
	current := l.head.next
	fmt.Printf("this is initial head point: %v\n", l.head.next)

	for i := uint(1); i <= step; i++ {
		//current代表要被处理的节点
		//下一个要处理的自然是当前处理current的next，使用tmp先暂存起来，方便后面赋值
		tmp := current.next
		//新生成的节点是下次要被处理的节点的下一个节点
		current.next = next
		//节点的value和next节点的指向是节点的两个属性，都要记得修改
		next = current
		//current的原来的下一节点是下次循环要被处理的节点
		current = tmp
	}
	//处理中间节点，分偶数和奇数进行考虑
	mid := current

	//分成左右两部分链表
	var left, right *ListNode  = next, nil
	if lLen % 2 == 0 {
		right = mid
	} else {
		//奇数时跳过中间的节点
		right = mid.next
	}

	for nil != left && nil != right {
		if left.GetValue().(string) != right.GetValue().(string) {
			//不同就不是回文链表，中断返回错误即可
			isPalindrome = false
			break
		}
		left = left.GetNext()
		right = right.GetNext()
	}

	//恢复链表
	//逆转左链表的第一个节点
	current = next
	//恢复的节点的下一个指向
	next = mid

	//或者用nil != current来做判断条件
	for i := uint(1); i <= step; i++ {
		tmp := current.next
		current.next = next
		next = current
		current = tmp
	}
	//注意我有哨兵，这个地址指向要兼顾到
	//开始错误将哨兵指向了nil，这样造成链表错误，而且还很不容易发现
	l.head.next = next
	//打印出来地址也没变啊，这里真的有必要做这一步操作吗？
	fmt.Printf("this is current head point: %v\n", l.head.next)

	return isPalindrome
}