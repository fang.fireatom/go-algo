package _6_linkeddist

import (
	"fmt"
	"testing"
)

//这里没有使用断言，而是通过输出人工判断，也算是节省人力
func TestInsertToHead(t *testing.T) {
	l := NewLinkedList()
	fmt.Println("哨兵节点值为0，链表长度为0：")
	fmt.Println(l.head.GetValue())
	fmt.Println(l.length)
	for i := 0; i < 10; i++ {
		l.InsertToHead(i)
	}
	l.Print()
}

func TestInsertToTail(t *testing.T) {
	l := NewLinkedList()
	for i := 0; i < 10; i++ {
		l.InsertToTail(i)
	}
	l.Print()
}

func TestDeleteNode(t *testing.T) {
	l := NewLinkedList()
	for i := 0; i < 3; i++ {
		l.InsertToTail(i)
	}
	l.Print()

	t.Log(l.DeleteNode(l.head.next))
	l.Print()

	t.Log(l.DeleteNode(l.head.next.next))
	l.Print()

	var ptr [32]byte
	//byte8位，超过255会报溢出
	ptr[0] = 255
	fmt.Println(len(ptr))
	fmt.Printf("%x\n", ptr[0])

	//同类型同长度才能比较
	//数组索引是int，下面这么写会将未定义的位置初始化为0
	months := [...]string{0:"name",3:"yuan"}
	//years := [...]string{"name", "yan"}
	//fmt.Println(months == years)
	fmt.Println(months[0])
	fmt.Println(len(months))
	fmt.Printf("%T\n", months)
}