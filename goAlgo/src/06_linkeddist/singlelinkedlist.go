package _6_linkeddist

import "fmt"

//todo 带验证的感悟：1.不一定说链表节点数据结构里的next一定是下一个node的地址（指针的概念），就是单纯的node类型也可以。
//单链表的节点
type ListNode struct {
	next  *ListNode
	value interface{}
}

//单链表
//很喜欢用指针，这一点我需要在理解消化消化
//head代表头节点，也就是整个链表的起点放，头节点充当哨兵节点，减少特殊情况下从nil初始化的麻烦
type LinkedList struct {
	head   *ListNode
	length uint
}

//单向链表，新节点没有指向下一节点
//配合的这个数据结构是没问题的，就是针对头节点的初始化存疑
func NewListNode(v interface{}) *ListNode {
	return &ListNode{nil, v}
}

//新的单向链表
//他这里设置的初始化的默认第一个节点是0，并且不计入整个链表中。
//是否初始化应该自定义初始值v，长度增为1更合适？
//这里第一个空节点代表哨兵指针，构成带头链表，为了简化增加和删除节点的特殊情况
func NewLinkedList() *LinkedList {
	return &LinkedList{NewListNode(0), 0}
}

//获取下个节点地址
func (this *ListNode) GetNext() *ListNode {
	return this.next
}

//获取当前节点值
func (this *ListNode) GetValue() interface{} {
	return this.value
}

//在某个节点后面插入节点
func (this *LinkedList) InsertAfter(p *ListNode, v interface{}) bool {
	if nil == p {
		return false
	}
	newNode := NewListNode(v)
	oldNext := p.next
	p.next = newNode
	newNode.next = oldNext
	this.length++
	return true
}

//在某个节点前插入节点
func (this *LinkedList) InsertBefore(p *ListNode, v interface{}) bool {
	//不允许在哨兵节点之前添加新节点
	//理解上就是哨兵节点实际不存在，所以不存在有前置节点指向它
	if nil == p || p == this.head {
		return false
	}
	current := this.head
	next := this.head.next
	for next != nil {
		if next == p {
			break
		}
		current = next
		next = current.next
	}
	//这里代表的是p节点不在链表中
	if next == nil {
		return false
	}
	newNode := NewListNode(v)
	current.next = newNode
	newNode.next = p
	this.length++
	return true
}

//删除某个节点
func (this *LinkedList) DeleteNode (p *ListNode) bool {
	if nil == p {
		return false
	}
	current := this.head
	next := current.next
	//循环到尾节点
	for next != nil {
		if next == p {
			break
		}
		current = next
		next = current.next
	}
	//表示遍历节点结束没有找到相应节点
	if next == nil {
		return false
	}
	current.next = p.next
	//释放存储
	p = nil
	this.length--
	return true
}

//在链表头部插入节点
//是链表需要提供的方法能力，使用上面已经完成好了的插入方法
func (this *LinkedList) InsertToHead(v interface{}) bool {
	return this.InsertAfter(this.head, v)
}

//在链表尾部插入节点
//链表提供的一个方法，这样写的时间复杂度铁定是n了，如果是从中加某个节点
//开始查找，时间会少一些
func (this *LinkedList) InsertToTail(v interface{}) bool {
	//这个哨兵节点的设置真的巧妙
	current := this.head
	for current.next != nil {
		current = current.next
	}
	return this.InsertAfter(current, v)
}

//通过索引查找节点
func (this *LinkedList) FindByIndex(index uint) *ListNode {
	if index >= this.length {
		return nil
	}
	//因为初始化时设定的头head是一个空的节点，第一个真正有意义并计入长度的点是头节点的下一个节点
	current := this.head.next
	for i := uint(0); i < index; i++ {
		//索引的前序节点的next是索引所在节点
		current = current.next
	}
	return current
}

//打印链表
func (this *LinkedList) Print() {
	//时刻记住添加了首部的哨兵空节点
	//第一个有效节点是哨兵节点之后的第一个节点
	current := this.head.next
	format := ""
	for nil != current {
		format += fmt.Sprintf("%+v", current.GetValue())
		current = current.next
		if nil != current {
			format += "->"
		}
	}
	fmt.Println(format)
}
