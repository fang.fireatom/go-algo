package _6_linkeddist

import "testing"

func TestPalindrome(t *testing.T) {
	strs := []string{"heooeh", "hello", "heoeh", "a", ""}
	for _, str := range strs {
		l := NewLinkedList()
		//构造链表结构
		for _, c := range str {
			l.InsertToTail(string(c))
		}
		l.Print()
		t.Log(isPalindrome(l))
	}
}