package main

import (
	"fmt"
	"testing"
)
var all int = 4
//iota的常量语法
const (
	FlagUp uint = 1 << iota // is up
	FlagBroadcast            // supports broadcast access capability
	FlagLoopback             // is a loopback interface
	FlagPointToPoint         // belongs to a point-to-point link
	FlagMulticast            // supports multicast access capability
)

//func TestComStruct(t *testing.T) {
//	var a = [3]int{1,2,3}
//	fmt.Println(a[len(a)-2])
//	fmt.Printf("%[1]v\n", a)
//	for _, v := range a {
//		fmt.Printf("%v\n", v)
//	}
//
//	//前面的代表初始化了107个0，索引是0-106，后面又附加两位，-1和8
//	r := [...]int{107:-1,8}
//	fmt.Println(r[107],len(r))
//
//	fmt.Printf("%v\n", FlagUp)
//	fmt.Printf("%v\n", FlagBroadcast)
//	fmt.Printf("%v\n", FlagLoopback)
//
//	fmt.Println(1 << 3)
//}

func TestChangeAll(t *testing.T) {
	y := 1 % 5
	fmt.Println(y)
	//s1 := []int{1,2,3,4,5,6}
	//s2 := make([]int, 6)
	//var c []int
	//c = append(c, 2)
	//fmt.Println(len(c))
	//fmt.Printf("s1:%v\n", s2[5])
	//copy(s2, s1)
	//fmt.Printf("s2:%v\n", s2)
	//s := "hello world"
	//fmt.Println(len(s))
	////这里的输出,[0:1]输出h的。[0]就输出h的ascall编码
	//fmt.Println(s[0:1], s[7])
	//fmt.Println(s[0:5])
	//
	//p := "Hello 世界！"
	//
	//for i, v := range p { // i 是字符的字节位置，v 是字符的拷贝
	//
	//	fmt.Printf("%2v = %c\n", i, v) // 输出单个字符
	//}
	//sy1, _ := strconv.Atoi("e")
	//sy1m, _ := strconv.Atoi("}")
	//fmt.Println(sy1)
	//fmt.Println(sy1m)
	//if x, err := strconv.Atoi("123"); err == nil {
	//	fmt.Printf("type: %T, value: %v\n strtype: %T, strvalue: %v\n", x,x,string(x),string(x))
	//}

}

func TestQuote(t *testing.T) {
	//m := &all
	//*m = 9
	//n := all
	//fmt.Printf("n is %v\n", n)
	//fmt.Printf("m is %v\n", *m)
	//fmt.Printf("all is %v\n", all)
	//
	//var nums []int
	////nums[3] = 8
	//fmt.Printf("len is %v, cap is %v, pointer is %p\n", len(nums), cap(nums), nums)
	//nums = append(nums, 3)
	//fmt.Printf("len is %v, cap is %v, pointer is %p\n", len(nums), cap(nums), nums)
}
func TestSlice(t *testing.T) {
	//data := make([]interface{},8)
	//data[0] = 3
	//fmt.Printf("%+v\n", data)
	//fmt.Println(data)
	//var n = 4.33
	//fmt.Printf("%g\n", n)
	//
	//p := new(int)
	//fmt.Printf("p value:%v,p type:%T\n",*p, *p)
	//
	////slice
	//var runes []rune
	//for _, i := range "yuanfang" {
	//	runes = append(runes, i)
	//}
	//fmt.Printf("%x\n", runes[0])
	//
	////切片初始化，给定某个长度，默认初始化是0值
	//initSlice := make([]int, 1, 32)
	//initSlice = append(initSlice, 2)
	//fmt.Println(initSlice[1])
	//fmt.Printf("all value is %v\n", all)

	//var ages map[string]int
	//ages = map[string]int{}
	//years := []string{"yuan", "fang"}
	//a := fmt.Sprintf("%q", years)
	//ages[a] = 56
	//fmt.Println(ages)
	//fmt.Println(a)
	//fmt.Printf("init len = %v\n", len(ages))
	//ages["yuan"] = 25
	//fmt.Printf("after value = %v\n", len(ages))
	//fmt.Printf("type:%T\n", ages["fang"])
	//if f, ok := ages["fang"]; !ok {
	//	fmt.Printf("not exist f, default value = %v\n", f)
	//}
	//
	//var p int = 2
	//n := &p
	//*n = 3
	//fmt.Printf("type n equal %T\n", n)
	//fmt.Println(p)
}