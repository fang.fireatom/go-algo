package _0_recursion

//递归公式f(n)=f(n-1)+f(n-2),终止条件f(1)=1,f(2)=2
//使用映射，hash表实现对已经计算的值的记录

//初始化一个map
//这种全局变量的生命周期
var borderValue = make(map[int]int)

//函数首字母大写才能被别的文件调用
func BorderRecursion(n int) int {
	if n == 1 { return 1 }
	if n == 2 { return 2 }

	//这里判断hash表里是否有，需要定义一个map，定义一个全局的map
	if value, ok := borderValue[n]; ok {
		return value
	}

	//多了一步记录的操作
	//递归的这一步记录已有值的操作简直太骚了，省去了很多重复计算
	ret := BorderRecursion(n-1) + BorderRecursion(n-2)
	borderValue[n] = ret
	return ret
}
