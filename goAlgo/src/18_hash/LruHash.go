package _8_hash

import "fmt"

/*
 1. 基于散列表和双向链表实现LRU内存淘汰算法。
 2. 散列表可以是数组形式，数组里面存放该位置链表的第一个节点地址
 3. 链表用于LRU淘汰排序，散列表用于查找的时间复杂度
 4. 是用链表来解决冲突，可以尝试使用线性探测
 5. 节点的v用来做数据的区分，简化成int值。hash函数要设计到映射到一定范围，为了不至于数组申请容量过大，这里限制一下数据范围。v用来判断是否重复进行缓存淘汰。
 6. 最简单的情况是没有冲突，一个v就在散列表里代表一个位置。我可以强行使用数字的个位做hash。如21，31在同一个位置上。hnext，pre和next各自负责各自的范围进行变动。
 */

//节点，hnext用于
type DualNode struct {
	pre   *DualNode
	next  *DualNode
	v     int
	hnext *DualNode
}

//直接用v来做hash键映射，因为我的目的主要是LRU缓存淘汰，不是简单的存储。没必要引入一个字符串类型的key
//设置哨兵，初始化的时候不用特殊化处理
//哨兵节点默认放在索引为0的位置
//为存储结构增加容量限制，实现存储满了之后的数据淘汰
type LruHashList struct {
	head     *DualNode
	tail     *DualNode
	data     []*DualNode
	capacity int
	length   int
}

//初始化一个节点，指针不明，都先写到nil
func NewDualNode(v int) *DualNode {
	return &DualNode{nil, nil, v, nil}
}

//初始化一个散列表结构
//配合简化的hash函数，设置切片长度容量都是10，索引0-9
func NewLruHashList(c int) *LruHashList {
	//默认设置的空头节点不算一个长度，哨兵我不放入散列表存储
	//初始化时头节点和尾节点都是哨兵节点
	head := NewDualNode(-1)
	//哨兵头节点不放入散列表存储
	//todo 为了代码的简洁，干脆设置两个没有实际意义的头部和尾部哨兵节点。具体还要分析是否必要：没必要设置尾节点的哨兵
	//下面的初始化就是ok的
	return &LruHashList{head, head, make([]*DualNode, 10), c, 0}
}

//简化版本hash算法，直接取余
func Hash_s(v int) int {
	return v % 10
}

//利用散列表降低查询的时间复杂度
//这里面的循环算是链表遍历的一个小技巧
func (this *LruHashList) Search(v int) *DualNode {
	k := Hash_s(v)
	current := this.data[k]
	//这么写简化很多了，只需要明确什么时候算找到，其余的几种情况就是找不到，都罗列出来就很繁琐了
	for current != nil {
		if current.v == v {
			return current
		}
		current = this.data[k].hnext
	}
	return nil
}

//插入数据的过程就是一个LRU过程，依附于我定义的哈希链表结构
//想单纯的实现一个函数的话那就外层写一个函数，调用这个数据结构即可
//最新的数据插入到存储末位
//todo 下面的逻辑还是清晰的，就是if else的使用看起来容易乱掉，这个是不是有更好的格式
func (this *LruHashList) LruInsert(v int) {
	lv := this.Search(v)
	n := NewDualNode(v)
	//存储中没有v
	if lv == nil {
		//要记得维护两个表中的变化
		if this.length < this.capacity {
			this.tail.next = n
			n.pre = this.tail
			this.tail = n
			hv := Hash_s(v)
			if this.data[hv] == nil {
				this.data[hv] = n
			} else {
				current := this.data[hv]
				for current.hnext != nil {
					current = current.hnext
				}
				//散列表这里的hnext只需要是单向的就行了
				current.hnext = n
			}
			//插入了新的元素，所以长度加1
			this.length++
		} else {
			//长度满了，需淘汰数据，从头部清除，哨兵节点的下一个节点要清除，长度不变
			//删除节点，双向链表
			dn := this.head.next
			this.head.next = dn.next
			dn.next.pre = this.head
			//在散列表中删除该节点
			dv := dn.v
			hdv := Hash_s(dv)
			current := this.data[hdv]
			//理论上应该就是一定找的到dv, 循环条件要考虑边界，测试真的是相当必要：单元测试和功能测试都要有
			for  current.hnext != nil {
				if current.v == dv {
					current.hnext = current.hnext.hnext
				}
				current = current.hnext
			}

			//todo hnext的遍历或者改成双向链表进行删除。一般来说散列函数设计的好，就单链表足够
			//尾部插入新节点
			this.tail.next = n
			n.pre = this.tail
			this.tail = n
			//插入散列表中
			hv := Hash_s(v)
			if this.data[hv] == nil {
				this.data[hv] = n
			} else {
				current := this.data[hv]
				for current.hnext != nil {
					current = current.hnext
				}
				//散列表这里的hnext只需要是单向的就行了
				current.hnext = n
			}
		}
	} else {
		//存储中有v
		//在双向链表中移动节点到末尾
		lv.pre.next = lv.next
		lv.next.pre = lv.pre
		this.tail.next = lv
		lv.pre = this.tail
		//这里的next指针nil化可能会忘
		lv.next = nil
		this.tail = lv
		//散列表中位置不需要变动
	}
}

//按照从前往后顺序打印存放的数据，越靠后数据越新
func (this *LruHashList) Print() {
	current := this.head
	for current != nil {
		fmt.Println(current.v)
		current = current.next
	}
}
