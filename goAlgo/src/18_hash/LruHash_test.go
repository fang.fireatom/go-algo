package _8_hash

import (
	"fmt"
	"testing"
)

func TestLruHashList_LruInsert(t *testing.T) {
	l := NewLruHashList(10)
	l.LruInsert(2)
	fmt.Println(l.length)
	l.LruInsert(12)
	fmt.Println(l.length)
	for i := 1; i < 10; i++ {
		l.LruInsert(i)
	}
	ln := l.Search(2)
	fmt.Println(ln.hnext.v)
	fmt.Println(l.tail.v)
	fmt.Println(l.length)
	l.LruInsert(12)
	fmt.Println(l.tail.v)
	l.LruInsert(13)
	l.Print()
}
