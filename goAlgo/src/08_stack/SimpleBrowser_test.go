package _8_stack

import "testing"

func TestNewBrowser(t *testing.T) {
	//我设计的是栈顶是当前页面
	b := NewBrowser()
	b.pushBack("www.baidu.com")
	b.pushBack("www.sina.com")
	b.pushBack("www.baishan.com")
	b.back()
	b.foward()
	b.back()
	b.back()
	if b.canBack() == false {
		t.Log("无法后退")
	}
	b.foward()
	b.open("www.taobao.com")
	b.back()
	b.foward()
}