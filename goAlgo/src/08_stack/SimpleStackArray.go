package _8_stack

//go和后类型的语言的写法有区别
type simpleArrayStack struct {
	//这是切片
	data []interface{}
	count int
	capacity int
}

func NewSimpleArrayStack(c int) simpleArrayStack {
	return simpleArrayStack{
		make([]interface{},c),
		0,
		c,
	}
}

//入栈
func (this *simpleArrayStack) push(v interface{}) bool {
	if this.count == this.capacity {
		return false
	}
	this.data[this.count] = v
	this.count++
	return true
}

//出栈
func (this *simpleArrayStack) pop() interface{} {
	if this.count == 0 {
		return nil
	}
	tmp := this.data[this.count-1]
	this.count--
	return tmp
}


