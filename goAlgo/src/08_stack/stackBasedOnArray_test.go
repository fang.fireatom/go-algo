package _8_stack

import (
	"fmt"
	"testing"
)

func TestArrayStack_Push(t *testing.T) {
	s := NewArrayStack(3)
	s.Push(2)
	s.Push(3)
	s.Push(4)
	fmt.Println(s.isFull())
	s.Print()
	s.Push(5)
	s.Print()
}

func TestArrayStack_Pop(t *testing.T) {
	s := NewArrayStack(3)
	s.Push(2)
	s.Push(3)
	s.Push(4)
	s.Push(5)
	fmt.Printf("top is: %v\n", s.Top())
	fmt.Printf("pop is: %v\n", s.Pop())
	fmt.Printf("pop is: %v\n", s.Pop())
	fmt.Printf("pop is: %v\n", s.Pop())
	fmt.Printf("pop is: %v\n", s.Pop())
	fmt.Printf("now top is %v\n", s.Top())
}
