package _8_stack

import "testing"

func TestNewListStack(t *testing.T) {
	newListStack := NewListStack()
	newListStack.ShowTime()
	for i := 0; i < 4; i++ {
		newListStack.Push(i)
	}
	newListStack.ShowTime()
	newListStack.Pop()
	newListStack.Pop()
	newListStack.Push(4)
	newListStack.ShowTime()
}
