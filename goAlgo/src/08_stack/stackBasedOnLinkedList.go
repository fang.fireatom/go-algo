package _8_stack

import (
	"fmt"
)

//出栈是往回找节点。故使用双向链表做基本的数据结构
//基于链表实现的栈我就不限制长度了
//考虑了栈的链表节点的复用

//双向链表节点结构
type ListNode struct {
	pre   *ListNode
	next  *ListNode
	value interface{}
}

//基于链表的栈数据结构
type ListStack struct {
	top    *ListNode
	length int
}

//初始化栈
//这个就可以将初始化的top指向nil
func NewListStack() *ListStack {
	return &ListStack{
		top:    nil,
		length: 0,
	}
}

//入栈
func (this *ListStack) Push(v interface{}) {
	//初始化一个新的node节点存储新的数据
	//考虑复用pop操作过后依然存在的node节点
	if this.top == nil {
		nextListNode := ListNode{
			pre:   this.top,
			next:  nil,
			value: v,
		}
		this.top = &nextListNode
		//这里应该可以优化，top为nil和top的next为nil时处理方式都一样
		//一个变量不存在的参数应该怎么去判定？
	} else if this.top.next == nil {
		nextListNode := ListNode{
			pre:   this.top,
			next:  nil,
			value: v,
		}
		//这一句漏掉的话并不能达到复用节点的作用，但是由于我所有的操作都只涉及到向前节点的追溯，所以漏掉这一句我也能完成栈的大部分功能
		this.top.next = &nextListNode
		this.top = &nextListNode
	} else {
		//fmt.Println("复用了")
		this.top = this.top.next
		this.top.value = v
	}
	this.length++
}

//出栈
func (this *ListStack) Pop() interface{} {
	if this.length == 0 {
		fmt.Println("栈为空")
		return nil
	}
	val := this.top.value
	this.top = this.top.pre
	this.length--
	return val
}

//打印栈里面的所有数据
func (this *ListStack) ShowTime() {
	if this.length == 0 {
		fmt.Println("栈为空")
	}
	li := this.top
	for li != nil {
		fmt.Println(li.value)
		li = li.pre
	}
}