package _8_stack

import "fmt"

//这里简化成{[(这三个字符,并且不考虑这几个字符之外的情况
//字符和索引的问题，索引是按照字节数来取，可能出现取出值不符合要求的作用
func isSymbolMatched(v string) bool {
	s := NewArrayStack(20)
	//建立左右符号map表结构
	sMap := map[string]string{
		"}": "{",
		"]": "[",
		")": "(",
	}
	for _, m := range v {
		//range遍历出来的是一个int32的整型，这个我看跟ascall，unicode，utf-8编码方式有关系
		//fmt.Printf("type: %T, char: %c ", m,m)
		n := string(m)
		if n == "{" || n == "[" || n == "(" {
			s.Push(n)
			fmt.Println(n)
		} else {
			//不匹配，右符号不存在的情况也要考虑
			if s.Pop() != sMap[n] || s.IsEmpty() {
				return false
			}
		}
	}
	//右括号过多没有左括号与之匹配
	if s.IsEmpty() {
		return true
	}
	return false
}
