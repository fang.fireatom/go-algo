package _8_stack

import "fmt"

/*
基于数组实现的栈
结构体中使用栈顶指针的形式，类似于索引，从0开始，0表示是栈里面最底层的数据
 */
 //这里引入一个堆栈的长度也能解决是否为空栈的问题
type  ArrayStack struct {
	data []interface{}
	top  int
}

//初始化容量为c的栈
//不支持动态扩容
//指针是为了减少值复制传递，提高内存效率
func NewArrayStack(c int) *ArrayStack {
	return &ArrayStack{
		data: make([]interface{}, 0, c),
		top:  -1,
	}
}

//判断栈是否为空
func (this *ArrayStack) IsEmpty() bool {
	if this.top < 0 {
		return true
	}
	return false
}

//判断是否满栈，支持动态扩容时以此为信号进行扩容
func (this *ArrayStack) IsFull() bool {
	if this.top < cap(this.data) - 1 {
		return false
	}
	return true
}

//压栈
func (this *ArrayStack) Push(v interface{}) bool {
	//初始化的时候是-1
	if this.top < 0 {
		this.top = 0
	} else if this.IsFull() {
		//动态扩容，2倍空间.自己写个扩容,如果指定长度是0就没法正常copy，看着是需要append才能正常添加数据
		NewData := make([]interface{}, 2*cap(this.data))
		//TODO copy使用存在问题
		copy(NewData, this.data)
		//todo 动态扩容这里存疑，append本来就做了类似的事情，这里有些重复
		//NewData[len(this.data)] = v,这样写我自己的动态扩容才有意义，append在有容量时是怎么做数据增加的，todo 还是存疑，不确定是否还会重新申请内存空间产生数据搬迁
		NewData = append(NewData, v)
		this.data = NewData
		this.top += 1
	} else {
		this.top += 1
	}
	//内部数据是通过一个slice存储
	if this.top > len(this.data) - 1 {
		//不限制IsFull，这里的append会自动帮我给slice扩容。go内部实现
		this.data = append(this.data, v)
	} else {
		this.data[this.top] = v
	}
	return true
}

//出栈
func (this *ArrayStack) Pop() interface{} {
	if this.IsEmpty() {
		return nil
	} else {
		p := this.data[this.top]
		this.top--
		return p
	}
}

//检查栈顶
func (this *ArrayStack) Top() interface{} {
	if this.IsEmpty() {
		return nil
	} else {
		return this.data[this.top]
	}
}

//清空栈
//只是设置了栈指针，实际的数据还是在内部的切片中存放着
func (this *ArrayStack)Flush() {
	this.top = -1
}

//打印栈中数据，不涉及指针改变，单纯是打印内部slice中的数据
func (this *ArrayStack)Print() {
	if this.IsEmpty() {
		fmt.Println("empty stack")
	} else {
		for i := this.top; i >=0; i-- {
			fmt.Println(this.data[i])
		}
	}
}

