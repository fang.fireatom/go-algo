package _8_stack

import (
	"fmt"
	"testing"
)

func TestSimpleArrayStack(t *testing.T) {
	simpleArrayStack := NewSimpleArrayStack(8)
	//入栈
	for i := 0; i < 8; i++ {
		simpleArrayStack.push(i)
		fmt.Println(simpleArrayStack.data[i])
	}
	fmt.Println("this", simpleArrayStack.count)
	//出栈
	for simpleArrayStack.count != 0 {
		fmt.Println(simpleArrayStack.pop())
	}
}
