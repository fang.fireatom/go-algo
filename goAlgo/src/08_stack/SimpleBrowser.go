package _8_stack

import "fmt"

//可以使用一个抽象的接口作为数据结构，这里还是使用固定的栈结构

//第一步是将问题抽象出来得到一个合适的数据结构，算法是操作数据结构的方法
//这里的浏览器可以抽象成两个栈结构
//结构出来后可以考虑怎么初始化结构体数据

//这里我是考虑栈顶就是当前页面，这么一看教程文档里面是错误的，在处理回退网页这里逻辑不对
type Browser struct {
	BackStack   ArrayStack
	FowardStack ArrayStack
}

//初始化浏览器结构
//自己写，注意类型
func NewBrowser() *Browser {
	return &Browser{
		BackStack: *NewArrayStack(10),
		FowardStack: *NewArrayStack(10),
	}
}

//插入一些访问记录
func (this *Browser) pushBack(v string) {
	//限定是interface，插入string也没问题
	this.BackStack.Push(v)
}

//是否可以回退
func (this *Browser) canBack() bool {
	if this.BackStack.top <= 0 {
		return false
	}
	return true
}

//是否还能前进
func (this *Browser) canFoward() bool {
	if this.FowardStack.IsEmpty() {
		return false
	}
	return true
}

//网页向前
func (this *Browser) foward() {
	if this.canFoward() {
		top := this.FowardStack.Pop()
		this.BackStack.Push(top)
		fmt.Printf("页面前进到：%v\n", top)
	}
	//这个我不return会有什么不同？
	return
}

//页面回退
func (this *Browser) back() {
	if this.canBack() {
		top := this.BackStack.Pop()
		this.FowardStack.Push(top)
		fmt.Printf("页面回退到：%v\n", this.BackStack.Top())
	}
	return
}

//打开新的网页
func (this *Browser) open(v string) {
	this.BackStack.Push(v)
	this.FowardStack.Flush()
	fmt.Printf("打开新的网页：%v\n", v)
}
