package _1_sorts

//合并排序的递归公式：merge_sort(A, p, r) = merge(merge_sort(A, p, q), merge(A, q+1, r)),q = (p+r)/2
//终止条件：p>=r
//不要用人的思维去想像这个递归过程，而是像明白这个递归的操作，根据递归公式实现相应的代码
//直接对数组排序，不使用返回值，不占用多余的存储空间
func MergeSort(arr []int, n int) {
	MergeSort_c(arr, 0, n-1)
}

//实际的递归函数
func MergeSort_c(arr []int, p, r int) {
	//终止条件
	if p >= r {
		return
	}
	//递归公式
	q := (p + r)/2
	MergeSort_c(arr, p, q)
	MergeSort_c(arr, q+1, r)
	//合并
	Merge(arr, p, q, r)
}

//将两个排好序的数组合并
func Merge(arr []int, p, q, r int) {
	tmp := make([]int, r-p+1)
	i := p
	j := q + 1
	k := 0
	//for的用法真的灵活
	for ; i <= q && j <= r; k++ {
		if arr[i] <= arr[j] {
			tmp[k] = arr[i]
			i++
		} else {
			tmp[k] = arr[j]
			j++
		}
	}
	//上面可以使用哨兵，比如在代排序的数组末尾都加上一个与最后一个数值相同的数值。我这里arr是原始数组，不能随意添加哨兵。for循环的&&换成||就可以完成整个数组到tmp的搬移

	//将剩余的数据迁移到tmp数组中
	//直接通过两个for循环做判断了，不需要使用if再做一次认证
	for ; i <= q; i++ {
		tmp[k] = arr[i]
		k++
	}

	for ; j <= r; j++ {
		tmp[k] = arr[j]
		k++
	}

	//将tmp数组中排好序的数据回写到原数组中
	copy(arr[p:r+1], tmp)
}
