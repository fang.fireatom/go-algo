package _1_sorts

import (
	"fmt"
	"testing"
)

func TestMergeSort(t *testing.T) {
	a := []int{2,2,4,1,6,3,4,6,8,9,0,2,3,5}
	fmt.Println("排序前：", a)
	MergeSort(a, len(a))
	fmt.Println("排序后：", a)
}
