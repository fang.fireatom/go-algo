package _1_sorts

//通过额外的空间实现计数排序
func CountingSort(a []int, n int) {
	//获取待排序数组的最大值，用以确定桶的范围
	maxValue := max(a)
	//初始值都是0
	c := make([]int, maxValue+1)

	//统计每个数值的数量
	//TODO 这里我开始错误使用了range遍历数组进行计数，得到了错误结果。
	for i := 0; i < n; i++ {
		c[a[i]]++
	}

	//依次累加
	for i := 1; i <= maxValue; i++ {
		c[i] = c[i-1] + c[i]
	}

	//初始化临时排序数组
	r := make([]int, n)
	for i := n-1; i >= 0; i-- {
		index := c[a[i]] - 1
		r[index] = a[i]
		c[a[i]]--
	}

	//拷贝数据到原数组
	for i := 0; i < n; i++ {
		a[i] = r[i]
	}
}

//获取数组中最大值
func max(a []int) int {
	max := a[0]
	for i := range a {
		if max < i {
			max = i
		}
	}
	return max
}