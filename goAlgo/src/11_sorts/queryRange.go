package _1_sorts

import "fmt"

//寻找第k大的数值
//n代表数组长度，k代表第k大数值
func QueryRank(arr []int, n, k int) int {
	if n <= k {
		fmt.Println("数组长度小于要求的k值")
		return 0
	}
	v := QueryRank_c(arr, 0, n-1, k)
	return v
}

//使用快排的思想，只不过递归的时候不是两边都去操作，而是根据实际情况选择需要进一步操作的数组部分
//递归公式：QueryRank(arr, p, r) = QueryRank(arr, p, q-1) + QueryRank(arr, q+1, r)，这个的意思不是算是相等，而是一个问题可以按照这个公式去拆解成两个规模更小的问题去解决
//终止条件是获取的中间值q = k + 1,此时arr[q]就是第k大的数字，因为索引下标是从0开始的
func QueryRank_c(arr []int, p, r, k int) int {
	//获取中间值
	q := partitionRank(arr, p, r)
	//终止条件
	if q+1 == k {
		return arr[q]
	} else if q+1 < k {
		 return QueryRank_c(arr, q+1, r, k)
	} else {
		return QueryRank_c(arr, p, q-1, k)
	}
}

//排序arr数组并且返回排序的中间索引
func partitionRank(arr []int, p, r int) int {
	//选取最后一个数字作为标杆值
	pivot := arr[r]
	//双指针原地交换排序
	i := p
	j := p
	//找第几大所以是逆排序
	for ; j < r; j++ {
		if arr[j] > pivot {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	//将标杆值交换到中间位置
	arr[i], arr[r] = arr[r], arr[i]
	return i
}