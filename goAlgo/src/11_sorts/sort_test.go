package _1_sorts

import (
	"fmt"
	"testing"
)

func TestSelectSort(t *testing.T) {
	a := []int{5,4,2,4,2,5,1,6}
	n := len(a)
	fmt.Println("排序前", a)
	SelectSort(a, n)
	fmt.Println("排序后", a)
}
