package _1_sorts

//快排的原理是将pivot值不断的放置到正确的位置上
//非原地排序的快速排序，使用额外的存储空间
//也可以不带返回值，这个区别主要是整体的实现思路,体现在递推公式中也不一样
//todo 递推公式QC(arr) = QC(less) + pivot + QC(more),这个的意图是最容易理解的递归。相当于不断的递归返回想要的结果。
func QuickSortExtra(arr []int) []int {
	//递归的终止条件：数组长度为0或者1时，此时不需要排序
	if len(arr) < 2 {
		return arr
	} else {
		pivot := arr[0]
		//这样定义切片，使用append函数避免了容量不够，拷贝数据浪费的时间
		//注意容量设定成len是为了保证容量一定够，长度设成0是为了使用append方法初始化slice中的元素
		l := make([]int, 0, len(arr))
		m := make([]int, 0, len(arr))
		for i:=1; i <= len(arr) - 1; i++ {
			if arr[i] < pivot {
				l = append(l, arr[i])
			} else {
				m = append(m, arr[i])
			}
		}
		//这里要使用递归，不要用人的思维去带入推断递归的过程，而是写出递推公式，设定终止条件，不满足终止条件的继续调用递归函数即可
		return append(append(QuickSortExtra(l), pivot), QuickSortExtra(m)...)
	}
}

//原地排序的快速排序，不需要返回值，直接操作原数组
//todo 递推公式QC(arr,p,r),这个思路根本不应该用等于号去书写。他是在不断的对待排数组进行排序，不断的缩小问题范围，不断的求解。拆解问题成QC(p,q-1)和QC(q+1,r),终止条件是p >= r(准确来说是拆解成的问题的左索引和右索引)
func QuickSortExtraNoReturn(arr []int, n int) {
	if n < 2 {
		//无需排序
		return
	}
	//做一层封装，使用户调用更简便
	QuickSortExtraNoReturn_c(arr, 0, n-1)
}

//递归函数，不带返回值，仅仅是不断的缩小问题域处理相应域的问题
//相当于一直在操作arr这个数组，不断的处理这个数组，递归完成时，数组的排序完成
func QuickSortExtraNoReturn_c(arr []int, start, end int) {
	//终止条件
	if start >= end {
		//表示这个拆分的问题无需处理
		return
	}
	//这里求分区点时也要带上上面的start和end参数，否则不能叫递归
	q := partitionExtra(arr, start, end)
	QuickSortExtraNoReturn_c(arr, start, q-1)
	QuickSortExtraNoReturn_c(arr, q+1, end)
}

//分区处理函数,使用了辅助存储空间
func partitionExtra(arr []int, start, end int) int {
	pivot := arr[end]
	l := make([]int, 0, end - start + 1)
	m := make([]int, 0, end - start + 1)
	for i := start; i < end; i++ {
		if arr[i] < pivot {
			l = append(l, arr[i])
		} else {
			m = append(m, arr[i])
		}
	}
	//重新赋值到原始的arr，这个思路实际上跟我的第一个快速排序方法的思路相同，只不过是没有通过返回值拼接的方式实现算法
	//这里面有问题啊，这种来回倒变量，切片操作，arr指向的不是最外层的那个地址空间。造成死循环问题。
	for i := 0; i < len(l); i++ {
		arr[start+i] = l[i]
	}
	//注意这是递归，下标是从start开始到end，不要总是操作成原始数组的0到end
	arr[start+len(l)] = pivot
	for i := 0; i < len(m); i++ {
		arr[start+len(l)+i+1] = m[i]
	}
	return start + len(l)
}



//不带返回值不使用额外的空间的递归排序
func QuickSortNoReturn(arr []int, n int) {
	if n < 2 {
		return
	}
	QuickSortNoReturn_c(arr, 0, n-1)
}

//递归逻辑，不使用额外的存储空间
func QuickSortNoReturn_c(arr []int, start, end int)  {
	//终止条件
	if start >= end {
		return
	}
	//原地排序外加返回分割点
	q := partition(arr, start, end)
	//递归调用相应函数进行排序
	QuickSortNoReturn_c(arr, start, q-1)
	QuickSortNoReturn_c(arr, q+1, end)
}

//双指针原地排序并且返回排序中间值
func partition(arr []int, start, end int) int {
	//选取最后一个数字作为标杆值
	pivot := arr[end]
	//双指针原地交换排序
	i := start
	j := start
	for ; j < end; j++ {
		if arr[j] < pivot {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	//将标杆值交换到中间位置
	arr[i], arr[end] = arr[end], arr[i]
	return i
}