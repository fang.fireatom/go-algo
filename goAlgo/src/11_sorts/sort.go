package _1_sorts

/*
冒泡排序，插入排序，选择排序
*/

//冒泡排序
// todo 切片在函数中被修改，原值中也能体现出来，变量的话可能需要传递指针？？
func BubbleSort(a []int, n int) {
	if n <= 1 {
		return
	}
	//i记录的是冒泡的次数，实际上只需要n-1次冒泡即可实现排序动作
	for i := 0; i < n - 1; i++ {
		//true标示需要退出
		flag := true
		//每次排序都会使末尾的一位得到正确的数字，即为n-1-i,如果当前排序是i，上一次就是i-1
		//那么此次需要搞定的位置是n-1-i，故边界的j+1 = n-1-i，所以j的边界条件是j<n-1-i
		for j := 0; j < n - i -1; j++ {
			if a[j] > a[j+1] {
				a[j], a[j+1] = a[j+1], a[j]
				//只要发生交换，说明没有完成排序
				flag = false
			}
		}
		if flag {
			break
		}
	}
}

//插入排序
func InsertionSort(a []int, n int) {
	if n <= 1 {
		return
	}
	for i := 1; i < n; i++ {
		value := a[i]
		//这里有个技巧，我得记录下来不需要交换的j的位置,可以考虑把j的值通过j=i-1表示，当a[j]<value时break出来
		//我这里的continue也是可以的
		for j := i - 1; j >= 0; j-- {
			if a[j] > value {
				//移动比要插入数字大的数字
				a[j+1] = a[j]
				continue
			}
			a[j+1] = value
		}
	}
}

//选择排序
func SelectSort(a []int, n int) {
	if n <= 1 {
		return
	}
	//排到最后一轮时，最后一个数字位置不需要变化
	for i := 0; i < n -1; i++ {
		j := i
		m := j
		val := a[j]
		//从当前位置到末尾比较得到最小的值
		for ; j < n; j++ {
			if a[j] < val {
				m = j
				val = a[j]
			}
		}
		//将最小值放置到已经排好序的队列的末尾
		a[i], a[m] = a[m], a[i]
	}
}


