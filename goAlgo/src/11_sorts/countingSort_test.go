package _1_sorts

import (
	"fmt"
	"testing"
)

func TestCountingSort(t *testing.T) {
	a := []int{1,7,5,6,6,7,3,1,2,0,4,9}
	fmt.Println("排序前：", a)
	CountingSort(a, len(a))
	fmt.Println("排序后：", a)
}
