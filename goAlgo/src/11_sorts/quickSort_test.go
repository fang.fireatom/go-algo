package _1_sorts

import (
	"fmt"
	"testing"
)

//这种非原地排序的算法是将排好序的数组以返回值的形式返回出来
//空间复杂度肯定不是O(1)了
func TestQuickSortExtra(t *testing.T) {
	a := []int{2,2,2,5,6,1,0,9,0,4,5}
	fmt.Println("排序前", a)
	a = QuickSortExtra(a)
	fmt.Println("排序后", a)
}

//这个不带返回值但是也使用了额外的存储空间
func TestQuickSortExtraNoReturn(t *testing.T) {
	a := []int{2,2,2,5,6,1,0,9,0,4,5}
	fmt.Println("排序前", a)
	QuickSortExtraNoReturn(a, len(a))
	fmt.Println("排序后", a)
}

//这个没有返回值并且没有使用额外的存储空间
func TestQuickSortNoReturn(t *testing.T) {
	a := []int{2,2,2,5,6,1,0,9,0,4,5}
	fmt.Println("排序前", a)
	QuickSortNoReturn(a, len(a))
	fmt.Println("排序后", a)
}