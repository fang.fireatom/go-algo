package _9_queue

import (
	"fmt"
	"testing"
)

func TestNewArrayQueue(t *testing.T) {
	q := NewArrayQueue(5)
	q.ShowTime()
	for i := 0; i < 5; i++ {
		q.EnQueue(i)
	}
	q.ShowTime()
	fmt.Println(q.DeQueue())
	q.EnQueue(0)
	q.ShowTime()
}
