package _9_queue

import "testing"

func TestCircularQueue(t *testing.T) {
	newCircularQueue := NewCircularQueue(5)
	newCircularQueue.ShowTime()
	for i := 0; i < 5; i++ {
		newCircularQueue.EnQueue(i)
	}
	newCircularQueue.ShowTime()
}
