package _9_queue

import "fmt"

//队列结构：队首指针，队尾指针，数组容量

//对列的数据结构
type ArrayQueue struct {
	data []interface{}
	capacity int
	head int
	tail int
}

//队列的初始化方法
func NewArrayQueue(n int) *ArrayQueue {
	return &ArrayQueue {
		data: make([]interface{},n),
		capacity: n,
		head: 0,
		tail: 0,
	}
}

//队列为空的判定
func (this *ArrayQueue) IsEmpty() bool {
	if this.tail == this.head {
		return true
	}
	return false
}

//队列满了的判定
func (this *ArrayQueue) IsFull() bool {
	if this.head == 0 && this.tail == this.capacity {
		return true
	}
	return false
}

//入队
func (this *ArrayQueue) EnQueue(v interface{}) bool {
	if this.IsFull() {
		return false
	}
	//尾指针指向队尾时需要做数据搬移
	if this.tail == this.capacity {
		for i := this.head; i < this.capacity; i++ {
			this.data[i-this.head] = this.data[i]
		}
		//这里的顺序不能错
		this.tail -= this.head
		this.head = 0
	}
	//正常的入队操作
	this.data[this.tail] = v
	this.tail++
	return true
}

//出队
func (this *ArrayQueue) DeQueue() interface{} {
	if this.IsEmpty() {
		return nil
	}
	t := this.data[this.head]
	this.head++
	return t
}

//额外提供一个打印队列中元素的方法
func (this *ArrayQueue) ShowTime() {
	if this.IsEmpty() {
		fmt.Println("队列为空")
		return
	}
	for i := this.head; i < this.tail; i++ {
		//这么写就不对了，这是出队，对队列有影响了
		//fmt.Println(this.DeQueue())
		fmt.Println(this.data[i])
	}
}
