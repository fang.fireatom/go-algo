package _9_queue

import "fmt"

//不存在队满的概念？
//出队之后，之前的链表节点怎么释放
//head=tail队空

//链表节点
type ListNode struct {
	value interface{}
	next  *ListNode
}

//链表队列
type ListQueue struct {
	head   *ListNode
	tail   *ListNode
	length int
}

//初始化链表队列
//其实把length用起来判断队列为空就不用直接比较head和tail了
func NewListQueue() *ListQueue {
	newListNode := ListNode{nil, nil}
	return &ListQueue{
		head:   &newListNode,
		tail:   &newListNode,
		length: 0,
	}
}

//判断队列是否为空
func (this *ListQueue) isEmpty() bool {
	if this.head == this.tail {
		return true
	}
	return false
}

//入队
//不限制队列长度时，入队都能成功，无需做验证
func (this *ListQueue) EnQueue(v interface{}) {
	nextNode := ListNode{nil, nil}
	this.tail.value = v
	this.tail.next = &nextNode
	this.tail = &nextNode
	this.length++
}

//出队
func (this *ListQueue) DeQueue() interface{} {
	if this.isEmpty() {
		fmt.Println("队列为空")
		return nil
	}
	val := this.head.value
	this.head = this.head.next
	this.length--
	return val
}

//打印队列中的数值
//不使用出队操作，因为出队会导致队列中数据的改变
func (this *ListQueue) ShowTime() {
	if this.isEmpty() {
		return
	}
	showNode := this.head
	for showNode != this.tail {
		fmt.Println(showNode.value)
		showNode = showNode.next
	}
}