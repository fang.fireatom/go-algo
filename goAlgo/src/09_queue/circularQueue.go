package _9_queue

import "fmt"

//循环队列，使用数组
//队空的判定是head=tail
//队满会浪费一个存储位置，条件是tail在head前一个位置，且tail处不存放数据，这样head=tail+1，但是考虑到是个环，当head是0时，tail+1%n余的就是0
//这样的取余也是满足简单的head=tail+1的

//循环队列结构
//限定队列长度，实际应用时队列长度肯定也是要限制的。就像线程数不能过多，否则会影响运行效率
type CircularQueue struct {
	data     []interface{}
	capacity int
	head     int
	tail     int
}

//初始化队列
func NewCircularQueue(n int) *CircularQueue {
	return &CircularQueue{
		data:     make([]interface{}, n),
		capacity: n,
		head:     0,
		tail:     0,
	}
}

//判断队列是否为空
func (this *CircularQueue) IsEmpty() bool {
	if this.head == this.tail {
		return true
	}
	return false
}

//判断是否满队
func (this *CircularQueue) IsFull() bool {
	if (this.tail + 1) % this.capacity == this.head {
		//tail为4，head为0，此时满足队满的条件，故容量为5的循环队列实际上只能存放4个数据
		fmt.Println(this.head,this.tail)
		return true
	}
	return false
}

//入队
func (this *CircularQueue) EnQueue(v interface{}) bool {
	if this.IsFull() {
		fmt.Println("队列已满")
		return false
	}
	this.data[this.tail] = v
	if this.tail == this.capacity - 1 {
		this.tail = 0
	} else {
		this.tail++
	}
	return true
}

//出队
func (this *CircularQueue) DeQueue() interface{} {
	if this.IsEmpty() {
		fmt.Println("队列为空")
		return false
	}
	val := this.data[this.head]
	if this.head == this.capacity - 1 {
		this.head = 0
	} else {
		this.head++
	}
	return val
}

//打印队列
func (this *CircularQueue) ShowTime() {
	if this.IsEmpty() {
		fmt.Println("队列为空，无需打印")
	}
	i := this.head
	for i != this.tail {
		fmt.Println(this.data[i])
		if i == this.capacity - 1 {
			i = 0
		} else {
			i++
		}
	}
}

