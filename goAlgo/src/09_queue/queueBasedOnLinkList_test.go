package _9_queue

import (
	"fmt"
	"testing"
)

func TestNewListQueue(t *testing.T) {
	newListQueue := NewListQueue()
	newListQueue.ShowTime()
	for i := 0; i < 5; i++ {
		newListQueue.EnQueue(i)
	}
	newListQueue.ShowTime()
	for i := 0; i < 4; i++ {
		fmt.Println(newListQueue.DeQueue())
	}
	newListQueue.ShowTime()
	newListQueue.DeQueue()
	newListQueue.DeQueue()
}
