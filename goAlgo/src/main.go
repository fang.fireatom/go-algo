package main

import (
	"fmt"
	"regexp"
)

type Student struct {
	id      int
	name    string
	address string
	age     int
}

type Array struct {
	data []int
	length  uint
}

type LinkList struct {
	next *LinkList
	value interface{}
}

func structTest0102() {
	//测试链表结构概念
	var linkList LinkList = LinkList{nil, 5}
	fmt.Println(linkList)
	fmt.Printf("value:%v, type:%T", linkList)

	//使用&T{...}创建struct，结果为指针类型
	var s1 *Student = &Student{102, "John", "Nanjing Road", 19}
	s1.id = 4
	fmt.Println(s1.id)
	fmt.Println("modifyStudentByPointer...")
	fmt.Println(s1)

	//使用T{...}创建struct，结果为value类型
	fmt.Println("-------------")
	var s2 Student = Student{103, "Smith", "Heping Road", 20}
	s2.id = 3;
	fmt.Println(s2.id)
	fmt.Printf("s2:%T\n", s2)
	fmt.Println("modifyStudent...")
	fmt.Println(s2)
	//创建并初始化一个struct时，一般使用【上述】两种方式

	//其它方式
	var s3 *Student = &Student{id: 104, name: "Lancy"}
	fmt.Printf("s3:%T,%s,%s,%d\n", s3, s3.name, s3.address, s3.age)
}

func NewArray(capacity uint) *Array {
	if capacity == 0 {
		return nil
	}
	return &Array{
		data: make([]int, capacity, capacity),
		length: 0,
	}
}

func (this *Array)Len() uint {
	return this.length
}

func (this *Array)isIndexOutOfRange(index uint) bool {
	if index >= uint(cap(this.data)) {
		return true
	}
	return false
}

func main() {
	//验证切片
	ll := []int{}
	if ll == nil {
		fmt.Println("yes")
	}

	fmt.Printf("ll type %v", ll)
	//验证else if的逻辑
	if 1 < 2 {
		fmt.Println(1)
	}
	if 1 < 3 {
		fmt.Println(2)
	}
	var less []int
	less = append(less, 1)
	fmt.Println(less,cap(less))
	more := make([]int, 5, 5)
	fmt.Printf("more pre address: %p\n", &more)
	more = append(more, 1)
	fmt.Printf("more after address: %p\n", &more)
	fmt.Println(more)
	structTest0102()
	a := make([]int, 5, 5)
	b := [5]int{}
	var c [5]int
	fmt.Printf("a:%v,%T\n", a, a)
	fmt.Printf("b:%v,%T\n", b, b)
	fmt.Printf("c:%v,%T\n", c, c)

	var x int
	var y *int = &x

	x = 19
	fmt.Println(x)
	fmt.Println(y)

	//看起来指针和值调用方法都可以使用
	testArray := NewArray(10)
	testArray.length = 3
	testArray.data[2] = 2
	fmt.Printf("testAarrayType: %T, value: %v, Out: %v\n", testArray, testArray, testArray.isIndexOutOfRange(23))
	fmt.Println(&testArray.data)

	//测试数组打印
	var format string
	printArray := [10]int{}
	for i := 0; i < len(printArray); i++ {
		printArray[i] = i
		format += fmt.Sprintf("|%+v", printArray[i])
	}
	fmt.Println(printArray)
	fmt.Println(format)

	//测试切片
	var vals []int
	for i := 0; i < 5; i++ {
		//selice的容量按照1 2 4 8 16的形式成倍增加
		vals = append(vals, i)
		fmt.Println("The length of our slice is:", len(vals))
		fmt.Println("The capacity of our slice is:", cap(vals))
	}
	// Add a new item to our array
	vals = append(vals, 123)
	fmt.Println("The length of our slice is:", len(vals))
	fmt.Println("The capacity of our slice is:", cap(vals))
	// Accessing items is the same as an array
	fmt.Println(vals[5])
	fmt.Println(vals[2])

	//测试struct
	var arrayList = []Array{
		//代表这里面的变量都是Array类型，可以类比[]int{}的初始化理解
		{[]int{1,2}, 2},
		{[]int{3,4,5}, 3},
	}
	fmt.Printf("arrayList:%v, type: %T", arrayList, arrayList)

	//测试编写的array类
	//arrayForTest := _05_array.NewArray(5)
	//arrayForTest.Print()
	//arrayForTest.Insert(0, 3)
	//arrayForTest.Print()
	//fmt.Println(arrayForTest.Len())
	//arrayForTest.Delete(0)
	//fmt.Println(arrayForTest.Len())

	//闭包函数
	clFun := func(i, j int) int { return i+j }
	fmt.Println(clFun(2,3))

	graph := make(map[int][]int)
	graph[1] = make([]int,3)
	fmt.Println(graph[1])

	//测试正则匹配
	regpId := regexp.MustCompile(`an=([0-9]*)`)
	reb := "a=w"
	fmt.Printf("%q",regpId.FindStringSubmatch(reb))
	fmt.Println(regpId.FindStringSubmatch(reb) == nil)

	ddd := []string{}
	fmt.Println(ddd)
	if len(ddd) == 0 {
		fmt.Println("ddd")
	} else if ddd[0] == "" {
		fmt.Println("kong")
	}
}

