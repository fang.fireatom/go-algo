package _5_binarysearch

/*
补充：指的是，将一个有序数组循环左/右移动若干距离之后变成的数组。如，[1,2,3,4,5]循环右移3位，就成为[4,5,1,2,3]。
该数组的特点是，其中包含着一个转折点。转折点左右两侧的子数组都是有序的，并且左侧的子数组整体都比右侧的子数组大。
我下面第一种实现的方法考虑了多种特殊情况，写起来实际上复杂很多了
后面的几种比较巧妙的写法就严格按照循序有序数组的定义去实现的
*/

/* 本篇使用三种不同的方法实现从循环有序数组中查找某个值
   如果数组的两个有序部分都有某个数值，这里就只返回第一个找到该数值的位置
   当然也可以获取所有的位置，这里为了简便就不做这部分功能的实现
*/

//查找分界点
//t为0代表数组递增，为1代表数组递减
func CircularSearch_b(arr []int, n, t, v int) int {
	//遍历寻找分界点
	i := 1
	for ; i < n; i++ {
		//后面等于条件是为了解决4 5 6 1 1 2 3这种分界点值重复的问题
		//我这里找的是后一个有序数组的起始下标
		if t == 0 {
			if arr[i] < arr[i-1] && arr[i] <= arr[i+1] {
				break
			}
		} else {
			if arr[i] > arr[i-1] && arr[i] >= arr[i+1] {
				break
			}
		}
	}
	if t == 0 {
		//先粗略的判断在哪个区间，然后本区间找不到，也有可能在另一个区间
		if v >= arr[0] && v <= arr[i-1] {
			//第一个区间找不到可能第二个区间有
			l := binarySearchSlice(arr, 0, i-1, v)
			if l == -1 {
				return binarySearchSlice(arr, i, n-1, v)
			} else {
				return l
			}
		} else if v >= arr[i] && v <= arr[n-1] {
			//不在第一个区间的范围，就只能从第二个区间去找
			return binarySearchSlice(arr, i, n-1, v)
		} else {
			return -1
		}
	} else {
		//递减的循环有序数组
		if v >= arr[i-1] && v <= arr[0] {
			l := binarySearchSlice_r(arr, 0, i-1, v)
			if l == -1 {
				return binarySearchSlice_r(arr, i, n-1, v)
			} else {
				return l
			}
		} else if v >= arr[n-1] && v <= arr[i] {
			return binarySearchSlice_r(arr, i, n-1, v)
		} else {
			return -1
		}
	}
	return -1
}

//正向数组的二分查找
func binarySearchSlice(arr []int, low, high, v int) int {
	mid := low + ((high - low) >> 1)
	for low <= high {
		//这个mid的变换很容易忘记导致代码陷入死循环
		mid = low + ((high - low) >> 1)
		if arr[mid] == v {
			return mid
		} else if arr[mid] < v {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	return -1
}

//反向数组的二分查找
func binarySearchSlice_r(arr []int, low, high, v int) int {
	mid := low + ((high - low) >> 1)
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] == v {
			return mid
		} else if arr[mid] < v {
			high = mid - 1
		} else {
			low = mid + 1
		}
	}
	return -1
}

/*这里实现的是一个比较巧妙的查找循环有序数组元素的方法
  上面的方法我定义的循环有序数组可以递增也能递减，这里限定的是必须是正向递增的数组
*/

//使用循环的思想实现
func CircularSearchCircle(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] == v {
			return mid
		} else if arr[mid] > arr[low] {
			//前面数组是有序的
			if mid == 0 { //这个边界情况要考虑，一般的二分不考虑是因为直接到外层的low <= high的认证去了，不会存在索引溢出问题
				return -1
			}
			if v >= arr[low] && v <= arr[mid-1] {
				return binarySearchSlice(arr, low, mid-1, v)
			} else {
				low = mid + 1
			}
		} else {
			//后面数组是有序的
			if mid == n - 1 {
				return -1
			}
			if v >= arr[mid+1] && v <= arr[high] {
				return binarySearchSlice(arr, mid+1, high, v)
			} else {
				high = mid -1
			}
		}
	}
	return -1
}

//使用递归的的思路实现
//判断的条件太多，并不是很适合用递归的形式，也没怎么简化
func CircularSearchRecursion(arr []int, n, v int) int {
	low := 0
	high := n - 1
	return circularSearch_r(arr, low, high, n, v)
}

func circularSearch_r(arr []int, low, high, n, v int) int {
	//终止条件，下面找到值return一个值也能算是终止条件
	//终止条件并不一定只有一个，我的理解
	if low > high {
		return -1
	}
	mid := low + ((high - low) >> 1)
	if arr[mid] == v {
		return mid
	} else if arr[mid] > arr[low] {
		if mid == 0 {
			return -1
		}
		if v >= arr[low] && v <= arr[mid-1] {
			return binarySearchSlice(arr, low, mid-1, v)
		} else {
			return circularSearch_r(arr, mid+1, high, n, v)
		}
	} else {
		if mid == n - 1{
			return -1
		}
		//这里出现了mid+1作为数组索引，要注意索引可能越界。遇到这种情况一定要警醒
		if v >= arr[mid+1] && v <= arr[high] {
			return binarySearchSlice(arr, mid+1, high, v)
		} else {
			return circularSearch_r(arr, low, mid-1, n, v)
		}
	}
}