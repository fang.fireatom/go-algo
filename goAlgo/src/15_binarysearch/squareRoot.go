package _5_binarysearch

import (
	"fmt"
	"strconv"
)

//使用二分法的思想求小数点后6位精度的平方根,等于的时候可以直接退出，不需要往下继续计算
//为了简便，这里约定求解的是整数的平方根
func SquareRoot(v int) float64 {
	//整数部分
	ri, flag := squareInt(v)
	if flag {
		return float64(ri)
	} else {
		return squareDec(float64(ri), float64(v))
	}
}

//二分法求解平方根的整数部分
//因为是平方比较，很容易就溢出了
func squareInt(v int) (int, bool) {
	low := 0
	high := v
	var mid int
	for {
		mid = (low + high)/2
		if mid * mid == v {
			//返回平方根整数部分，true表明整数部分即为平方根最终结果
			return mid, true
		} else if mid * mid < v  {
			if (mid+1)*(mid+1) == v {
				return mid+1, true
			} else if (mid+1)*(mid+1) > v {
				//找到了整数部分，false表示还需求解小数部分
				return mid, false
			} else {
				//todo 这里加2是更合适的 大错特错，我要找的是本身小，加1大的数字，这里直接加2，很可能直接跳到大于的数上，而我也只检验这个数往上一个的大小。
				low = mid + 1
			}
		} else if mid * mid > v {
			high = mid - 1
		}
	}
}

//求解小数部分，精确到小数点后6位
//浮点数的运算会存在精度问题，带的有小尾巴
func squareDec(squInt, v float64) float64 {
	k := 0.1
	tmp := squInt
	for k > 1e-7 {
		high := 10
		low := 0
		var mid int
		for low <= high {
			mid = (low + high)/2
			if (tmp+float64(mid)*k) * (tmp+float64(mid)*k) == v {
				break
			} else if (tmp+float64(mid)*k) * (tmp+float64(mid)*k) < v {
				//这么写是为了少二分查找一些次数
				if (tmp+float64(mid+1)*k) * (tmp+float64(mid+1)*k) == v {
					mid += 1
					break
				} else if (tmp+float64(mid+1)*k) * (tmp+float64(mid+1)*k) > v {
					//表明在k的这个精确度下找到了合适的位置
					break
				} else {
					low = mid + 1
				}
				//这些写if条件单纯是为了更清晰些，只写else也是一样的
			} else if (tmp+float64(mid)*k) * (tmp+float64(mid)*k) > v {
				high = mid - 1
			}
		}
		tmp += float64(mid)*k
		fmt.Println(tmp, k)
		//float运算会有一定的精度问题
		k, _ = strconv.ParseFloat(fmt.Sprintf("%.6f", k*0.1), 64)
	}
	//float的运算存在精度问题，这里找了个简单的方式调整精度，具体后面还要优化
	tmp, _ = strconv.ParseFloat(fmt.Sprintf("%.6f", tmp), 64)
	return tmp
}

