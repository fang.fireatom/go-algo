package _5_binarysearch

import (
	"fmt"
	"testing"
)

func TestSimpleBinary(t *testing.T) {
	a := []int{1,2,4,5,6,7,8,9,10,11}
	fmt.Println("location:", SimpleBinary(a, 9))
}

func TestSimpleBinary_r(t *testing.T) {
	a := []int{1,2,4,5,6,7,8,9,10,11}
	fmt.Println("location:", SimpleBinary_r(a, len(a), 4))
}
