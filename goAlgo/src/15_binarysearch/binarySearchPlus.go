package _5_binarysearch

//寻找第一次出现的位置，简洁实现方法
//确实不太好理解，特别是细想为何会有这样的效果的时候
func FirstSearch_s(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] >= v {
			high--
		} else {
			low++
		}
	}
	if low < n && arr[low] == v {
		return low
	} else {
		return -1
	}
}

//寻找第一次出现的位置，更容易理解的实现方式
func FirstSearch(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] < v {
			low = mid + 1
		} else if arr[mid] > v {
			high = mid - 1
		} else {
			if mid == 0 || arr[mid - 1] != v {
				//也可以一路从这个mid往下找下去，直到找到不为v的那个位置
				return mid
			} else {
				//只需要检验到等值的上一位即可，后面的查找交给二分查找的逻辑即可。
				high = mid - 1
			}
		}
	}
	return -1
}

//寻找最后一次出现的位置，简洁实现方法
//是上面找第一个的变种，思想一致，就是不那么好理解
func LastSearch_s(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] <= v {
			low++
		} else {
			high--
		}
	}
	if high < n && arr[high] == v {
		return high
	} else {
		return -1
	}
}

//寻找最后一次出现的位置，更容易理解的实现方式
func LastSearch(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] < v {
			low = mid + 1
		} else if arr[mid] > v {
			high = mid - 1
		} else {
			//这个是mid到达最大索引
			if mid == n - 1 || arr[mid + 1] != v {
				//也可以一路从这个mid往上找下去，直到找到不为v的那个位置
				return mid
			} else {
				//只需要检验到等值的下一位即可，后面的查找交给二分查找的逻辑即可。
				low = mid + 1
			}
		}
	}
	return -1
}

//寻找大于等于给定元素的第一个元素的位置
//原则就是规定好找到的条件，没找到就修改相应的参数修改二分查找的参数将查找过程交还给二分查找的逻辑
func LargeSearch(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] >= v {
			if mid == 0 || arr[mid - 1] < v {
				return mid
			} else {
				high = mid - 1
			}
		} else {
			low = mid + 1
		}
	}
	return -1
}

//查找最后一个小于等于给定元素的元素的位置
func LittleSearch(arr []int, n, v int) int {
	low := 0
	high := n - 1
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] <= v {
			if mid == n - 1 || arr[mid + 1] > v {
				return mid
			} else {
				low = mid + 1
			}
		} else {
			high = mid - 1
		}
	}
	return -1
}

