package _5_binarysearch

//基于循环的简单二分法实现
func SimpleBinary(arr []int, v int) int {
	low := 0
	high := len(arr) - 1
	//防止溢出，移位来提高效率
	var mid int
	for low <= high {
		mid = low + ((high - low) >> 1)
		if arr[mid] == v {
			return mid
		} else if arr[mid] < v {
			//这里不能写mid，否则可能会死循环
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	//-1表示没有匹配的数据
	return -1
}

//基于递推的简单二分法实现
//递推公式 SimpleBinary_r(arr, low, high) = SimpleBinary_r(arr, mid + 1, high) or SimpleBinary_r(arr, low, mid - 1)
//终止条件 low > high
func SimpleBinary_r(arr []int, n, v int) int {
	return SimpleBinary_rc(arr, 0, n-1, v)
}

//最开始漏写了终止条件
func SimpleBinary_rc(arr []int, low, high, v int) int {
	//这个终止条件已经写错不止一次了
	if low > high {
		return -1
	}
	mid := (low + high)/2
	if arr[mid] == v {
		return mid
	} else if arr[mid] < v {
		return SimpleBinary_rc(arr, mid + 1, high, v)
	} else {
		return SimpleBinary_rc(arr, low, mid - 1, v)
	}
}
