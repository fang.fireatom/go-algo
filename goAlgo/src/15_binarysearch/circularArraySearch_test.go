package _5_binarysearch

import (
	"fmt"
	"testing"
)

//通过人为判断循环数组是递增还是递减
func TestCircularSearch_b(t *testing.T) {
	arr := []int{4,5,6,1,2,3,4,6}
	//这个写法只能返回一个位置，优先前面数组，并且也不保证返回的是前面数组中的第一个或者最后一个出现该元素的位置
	fmt.Println("值所在的位置：", CircularSearch_b(arr, len(arr),0,4))
	arr_r := []int{3,2,1,6,5,4}
	fmt.Println("值出现的位置：", CircularSearch_b(arr_r, len(arr_r), 1, 2))
}


//只能用于查找严格符合定义的循环有序数组
func TestCircularSearchCircle(t *testing.T) {
	arr := []int{5,7,8,1,2,3}
	fmt.Println("值所在的位置", CircularSearchCircle(arr, len(arr), 4))
}

func TestCircularSearchRecursion(t *testing.T) {
	arr := []int{5,7,8,1,2,3}
	fmt.Println("值所在的位置", CircularSearchRecursion(arr, len(arr), 3))
}