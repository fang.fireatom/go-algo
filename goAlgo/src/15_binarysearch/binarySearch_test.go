package _5_binarysearch

import (
	"fmt"
	"testing"
)

func TestFirstSearch_s(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,6,7,7,8}
	fmt.Println("第一次出现位置：", FirstSearch_s(a, len(a), 2))
}

func TestLastSearch_s(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,6,7,7,8}
	fmt.Println("最后一次出现位置：", LastSearch_s(a, len(a), 7))
}

func TestFirstSearch(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,6,7,7,8}
	fmt.Println("第一次出现位置：", FirstSearch(a, len(a), 7))
}

func TestLastSearch(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,6,7,7,8}
	fmt.Println("最后一次出现位置：", LastSearch(a, len(a), 7))
}

func TestLargeSearch(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,7,7,8}
	fmt.Println("第一个大于等于给定值的元素位置：", LargeSearch(a, len(a), 9))
}

func TestLittleSearch(t *testing.T) {
	a := []int{1,2,3,4,5,5,5,7,7,8}
	fmt.Println("最后一个小于等于给定值的元素位置：", LittleSearch(a, len(a), 7))
}