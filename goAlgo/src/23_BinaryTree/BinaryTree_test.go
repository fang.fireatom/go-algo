package _3_BinaryTree

import (
	"fmt"
	"testing"
)

func TestBinaryTree_Insert(t *testing.T) {
	bt := NewBinaryTree(5)
	fmt.Println("前序遍历：")
	bt.PreOrder()
	fmt.Println("前序遍历迭代实现：")
	bt.PreOrderIteration()
	fmt.Println("中序遍历：")
	bt.InOrder()
	fmt.Println("中序遍历迭代实现：")
	bt.InOrderIteration()
	fmt.Println("后序遍历：")
	bt.PostOrder()
	fmt.Println("后序遍历迭代1：")
	bt.PostOrderIterationOne()
	fmt.Println("后序遍历迭代2：")
	bt.PostOrderIterationTwo()
	fmt.Println("按层遍历：")
	bt.LayerOrder()
}
