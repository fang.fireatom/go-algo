package _3_BinaryTree

import (
	"fmt"
	"09_queue"
)

/*
1. 用链表结构表示二叉树
2. 用递归实现前序，中序和后序遍历
3. 使用0-100随机树生成一个二叉树，我规定节点数。
 */

//二叉树结构
type BinaryTree struct {
	root *Node
}

//写一个插入函数，为了简化，干脆写成二叉查找树
//简化我的二叉树最少有个初始化的根节点
func (this *BinaryTree) Insert(v int) {
	t := this.root
	for t != nil {
		if v >= t.v {
			if t.right == nil {
				rn := NewNode(v)
				t.right = rn
				//这里不返回就死循环了，涉及链表的相关操作一定要注意
				//break
				//用return更好，防止循环后面还有代码被错误执行，到这里的时候就应该不在执行这个函数，应该直接返回
				return
			}
			t = t.right
		} else { //v < t.v
			if t.left == nil {
				ln := NewNode(v)
				t.left = ln
				//break
				return
			}
			t = t.left
		}
	}
}

//简单起见，直接初始化就指定二叉树的所有节点树,n代表除了根节点之外还有多少节点
func NewBinaryTree(n int) *BinaryTree {
	bt := &BinaryTree{NewNode(GetRandomValue())}
	for i := 0; i < n; i++ {
		bt.Insert(GetRandomValue())
	}
	return bt
}

//前序遍历
func (this *BinaryTree) PreOrder() {
	t := this.root
	PreOrder_r(t)
}

//迭代实现的前序遍历
func (this *BinaryTree) PreOrderIteration() {
	t := this.root
	PreOrderIteration(t)
}

//中序遍历
func (this *BinaryTree) InOrder() {
	t := this.root
	InOrder_r(t)
}

//迭代事项的中序遍历
func (this *BinaryTree) InOrderIteration() {
	t := this.root
	InOrderIteration(t)
}

//后序遍历
func (this *BinaryTree) PostOrder() {
	t := this.root
	PostOrder_r(t)
}

//迭代实现后序遍历1
func (this *BinaryTree) PostOrderIterationOne() {
	t := this.root
	PostOrderIterationStack_o(t)
}

//迭代实现后序遍历2
func (this *BinaryTree) PostOrderIterationTwo() {
	t := this.root
	PostOrderIterationStack_t(t)
}

//使用队列实现的按层遍历
func (this *BinaryTree) LayerOrder() {
	//最好能在二叉树的结构里面给一个节点数目，申请队列大小不会出现浪费和不足，同时也不会发生数据搬移。
	//但是二叉树结构里面写上长度也不现实，倒不如写一个可以动态扩容的队列结构
	q := _9_queue.NewCircularQueue(100)
	q.EnQueue(this.root)
	for !q.IsEmpty() {
		//已经转换成了interface类型，通过断言来恢复到需要的类型(告诉编译器变量类型，这么说应该更合适)，最好要捕捉这个异常去做异常处理。这里算法方面为了简便就直接简单的这么使用了
		bn := q.DeQueue().(*Node)
		fmt.Println(bn.v)
		if bn.left != nil {
			q.EnQueue(bn.left)
		}
		if bn.right != nil {
			q.EnQueue(bn.right)
		}
	}
}