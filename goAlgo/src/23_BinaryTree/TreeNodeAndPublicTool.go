package _3_BinaryTree

/*
1.存放工作的节点结构和可能公共使用到的一些函数
2.几个遍历的迭代实现的理解不是很顺畅，但是整体思路应该是用迭代来模拟递归的过程，迭代和递归需要继续理解练习
 */

import (
	"08_stack"
	"fmt"
	"math/rand"
	"time"
)

//树节点结构
type Node struct {
	v     int
	left  *Node
	right *Node
}

//产生随机的数字用于生成
func GetRandomValue() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(100)
}

//初始化节点
func NewNode(v int) *Node {
	return &Node{
		v,
		nil,
		nil,
	}
}

//递归实现前序遍历
//todo 究竟该如何理解递归，这里存在很大疑问，什么递推公式或者终止条件之类的在情况比较隐晦时不容易找到。
func PreOrder_r(p *Node) {
	//if p == nil {
	//	return
	//}
	if p != nil {
		fmt.Println(p.v)
		PreOrder_r(p.left)
		PreOrder_r(p.right)
	}
}

/*
  迭代实现前序遍历
  1.想清楚怎么使用迭代实现递归的逻辑，拆分步骤再转换成代码
 */
func PreOrderIteration(p *Node) {
	sk := _8_stack.NewArrayStack(100)
	n := p
	for n != nil || !sk.IsEmpty() {
		//一直向左子树遍历，打印节点并将节点值压入栈
		for n != nil {
			fmt.Println(n.v)
			sk.Push(n)
			n = n.left
		}

		//转向右子树进行处理
		if !sk.IsEmpty() {
			n = sk.Pop().(*Node)
			n = n.right
		}
	}
}

//递归实现中序遍历
func InOrder_r(p *Node) {
	if p == nil {
		return
	}
	InOrder_r(p.left)
	fmt.Println(p.v)
	InOrder_r(p.right)
}

/*
  迭代实现中序遍历
  1.先打印左子树，对比上面的前序，需要把打印节点的位置改一下
 */
func InOrderIteration(p *Node) {
	n := p
	sk := _8_stack.NewArrayStack(100)
	for n != nil || !sk.IsEmpty() {
		//把所有的左子树压入栈，直到最后一个节点，打印顺序应该是从最后一个左子树开始
		for n != nil {
			sk.Push(n)
			n = n.left
		}

		//转向右子树处理
		if !sk.IsEmpty() {
			n = sk.Pop().(*Node)
			fmt.Println(n.v)
			n = n.right
		}
	}
}
 
//递归实现后序遍历
func PostOrder_r(p *Node) {
	if p == nil {
		return
	}
	PostOrder_r(p.left)
	PostOrder_r(p.right)
	fmt.Println(p.v)
}

/*
  线性法实现后序遍历，参数是根节点
  这是比较省内存但是不是很好理解的一种写法，还有一种使用两个栈来实现的比较好理解的后序遍历
  思路理解：找一个树遍历尝试去理解，总体原则是一个节点是不是要打印就是要当它成为栈顶元素时去处理，pre代表上一个栈顶，根据现有栈顶和前一个栈顶的关系来确定如何处理当前的栈顶
  1.利用栈的先入后出特性，cur是pre的子节点需要继续遍历cur将cur子节点放入栈中，是先放左子树再放右子树
  2.pre是cur的右子树和pre与cur相同时才pop出cur，相等时说明处理的cur节点是叶子节点
  3.pre是cur左子树时，需要将cur的右子树压入栈
 */
func PostOrderIterationStack_o(p *Node) {
	//使用栈的概念,基于数组的支持动态扩容的栈
	sk := _8_stack.NewArrayStack(100)
	var cur, pre *Node
	//根节点先压入栈
	sk.Push(p)
	for !sk.IsEmpty() {
		cur = sk.Top().(*Node)
		//栈顶是根节点，cur是pre的子节点，要继续往下遍历cur的节点，直到找到cur子树中的叶子节点
		if pre == nil || cur == pre.left || cur == pre.right {
			//优先放左子树，没有则放右子树，但是这里只压入一个，否则处理的顺序就会出错
			if cur.left != nil {
				sk.Push(cur.left)
			} else if cur.right != nil {
				sk.Push(cur.right)
			}
		} else if cur.left == pre { //如果pre是cur的左子树，说明cur的左子树已经接受了判断处理，所以需要压入cur的右子树进行处理
			if cur.right != nil {
				sk.Push(cur.right)
			}
		} else {
			fmt.Println(cur.v)
			sk.Pop()
		}
		pre = cur
	}
}

/*
  两个栈配合实现的后序遍历，思路比较简单清晰一些
  1.s1队列来决定谁先入栈：本节点--右节点--左节点
  2.简单理解是处理一个节点时先把自己压入栈，然后是右子树节点，最后是左子树节点。
  3.虽说是迭代，但是还是能一直感受到递归的思想，比如迭代遍历的每一个节点的处理方法核心都是一样的
 */
func PostOrderIterationStack_t(p *Node) {
	s1 := _8_stack.NewArrayStack(100)
	s2 := _8_stack.NewArrayStack(100)
	n := p

	for n != nil || !s1.IsEmpty() {
		//遍历节点的右子树
		for n != nil {
			s1.Push(n)
			s2.Push(n)
			n = n.right
		}

		//转向左子树进行相关处理
		if !s1.IsEmpty() {
			n = s1.Pop().(*Node)
			//转向左子树，n这个点已经存放到了s2中，相当于已经处理过了
			n = n.left
		}
	}

	for !s2.IsEmpty() {
		n = s2.Pop().(*Node)
		fmt.Println(n.v)
	}
}