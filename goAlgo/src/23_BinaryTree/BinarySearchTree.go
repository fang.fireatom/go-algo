package _3_BinaryTree

/*
1. 简化起见，节点存放int值，并且不会出现相同的值
2. 具有插入，查找，删除功能
3. 除了插入考虑在树不存在时的初始化，其他的操作都默认树不为nil
 */

//二分查找树结构
type BinarySearchTree struct {
	root    *Node
}

//初始化二分查找树,初始化就要给一个根节点的值，不要后面insert再去处理这种没有初始化的问题
func NewBinarySearchTree(v int) *BinarySearchTree {
	return &BinarySearchTree{NewNode(v)}
}

//插入操作
func (this *BinarySearchTree) Insert(v int) {
	//if this == nil {
	//	//初始化一个二叉查找树
	//	//TODO 这个写法根本没法初始化一个树
	//	this = &BinarySearchTree{NewNode(v)}
	//	return
	//}
	p := this.root
	for p != nil {
		if p.v > v {
			if p.left == nil {
				ln := NewNode(v)
				p.left = ln
				return
			}
			p = p.left
		} else { //p.v >= v 人为保证不给重复值，虽然给了重复值也能正常插入，但是后面删除的时候会无法完全删除
		    if p.right == nil {
		    	rn := NewNode(v)
		    	p.right = rn
				return
			}
		    p = p.right
		}
	}
}

//查找操作
func (this *BinarySearchTree) Search(v int) (*Node) {
	if this == nil {
		return nil
	}
	p := this.root
	for p != nil {
		if p.v == v {
			return p
		} else if p.v < v {
			p = p.right
			} else {
				p = p.left
		}
	}
	return nil
}

//支持找到所有重复数据的搜索方法
func (this *BinarySearchTree) SearchRe(v int) []*Node {
	var ng []*Node
	if this == nil {
		//这个判断是认为二分查找树未初始化
		return nil
	}
	p := this.root
	for nil != p {
		if p.v == v {
			ng = append(ng, p)
			//叶子节点可以退出了，否则还应该在右子树中进行查找
			if p.right == nil && p.left == nil {
				return ng
			}
			p = p.right
		} else if p.v < v {
			p = p.right
		} else {
			p = p.left
		}
	}
	return ng
}

/*
1. 根据节点值删除节点
三种情况：
1. 节点没有子节点
2. 节点有一个子节点
3. 节点有两个子节点
4. 思路：通用意义上要找到需要删除的节点和节点的父节点，对于节点有两个子节点的情况还需要寻找其右子树中的最小节点
5. 注意我的树结构里面把根节点写到数据结构里了，所以只要是删除根节点就需要单独列出来处理
 */
func (this *BinarySearchTree) Delete(v int) bool {
	//寻找待删除节点及其父节点，没有复用上面的search函数，复用的话上面的search函数需要改造
	var pn *Node
	n := this.root
	//循环的条件要考量，变量的设置也要考量，要简化，避免复杂
	//这么写就简化了循环的边界，并且可以找到本节点和节点的父节点
	for n != nil && n.v != v {
		pn = n
		if n.v > v {
			n = n.left
		} else {
			n = n.right
		}
	}
	//没找到v节点，自然删除失败了
	if n == nil {return false}

	////节点没有子节点
	//if n.left == nil && n.right == nil {
	//	if pn == nil { //要删除的节点是根节点
	//	    this.root = nil
	//	}
	//	if pn.right == n {
	//		pn.right = nil
	//	} else { //pn.left = n
	//		pn.left = nil
	//	}
	//	return true
	//}

	//节点有两个子节点
	if n.left != nil && n.right != nil {
		//右子树上最小节点
		minP := n.right
		//右子树最小节点的父节点
		minPP := n
		//这里的遍历判断条件我错过很多回了！！！minP != nil完全错误
		for minP.left != nil {
			minPP = minP
			minP = minP.left
		}
		//此种节点的删除是替换要删除节点的值
		n.v = minP.v
		//删除右子树中最小的节点
		minPP.left = minP.right
		return true
	}

	//这里和上面叶子节点的删除理论上可以放到一个逻辑里面
	//这里的逻辑是删除只有一个子节点和没有子节点的节点
	var cn *Node
	//节点只有一个子节点
	if n.right != nil {
		cn = n.right
	} else if n.left != nil {
		cn = n.left
	} else {
		cn = nil
	}

	//删除的是根节点需要单独处理
	if pn == nil {
		this.root = cn
		return true
	}
	if pn.right == n {
		pn.right = cn
	} else {
		pn.left = cn
	}
	return true
}

/*
  支持删除重复数据的方法
  1.考量：上面的search方法是可以取得所有节点数据，但是按顺序删除节点时可能会改变已查询出来的节点状况
  2. 比如一个节点是双子节点节点，如果删除他就会把右子树上最小的节点删除，并替换到自己的位置上，如果这个最小节点刚好等于自身，那么删除就会有问题
  3. 倒不如多次调用普通的删除函数，直到返回值为false
 */
func (this *BinarySearchTree) DeleteRe(v int) bool {
	f := this.Delete(v)
	//记录是否重复删除成功
	rf := false
	for f == true {
		f = this.Delete(v)
		rf = true
	}
	return rf
}

//寻找最小节点
func (this *BinarySearchTree) MinNode() *Node {
	p := this.root
	for nil != p.left {
		p = p.left
	}
	return p
}

//寻找最大节点
func (this *BinarySearchTree) MaxNode() *Node {
	p := this.root
	for nil != p.right {
		p = p.right
	}
	return p
}

//中序遍历
func (this *BinarySearchTree) InOrder() {
	rn := this.root
	InOrder_r(rn)
}

