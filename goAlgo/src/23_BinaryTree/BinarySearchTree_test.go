package _3_BinaryTree

import (
	"fmt"
	"testing"
)

func TestBinarySearchTree(t *testing.T) {
	//var bt BinarySearchTree
	//bt.Insert(0)
	//bt.Insert(9)
	bt := NewBinarySearchTree(9)
	bt.Insert(19)
	for i := 0; i < 10; i++ {
		bt.Insert(GetRandomValue())
	}
	bt.Insert(19)
	bt.Insert(19)
	fmt.Println(bt.Search(9))
	fmt.Println(bt.Search(19))
	bt.InOrder()
	fmt.Println(bt.SearchRe(19))
	bt.Delete(19)
	fmt.Println(bt.SearchRe(19))
}
