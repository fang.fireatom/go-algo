import random

MAX_LEVEL = 16
INDEX_P = 0.5


class Node:
    """
    node节点是skiplist基本组成元素，包含加权值（排序使用），实际数据（简单起见，不另外设置，使用加权值），索引
    数据（加权）data:int, 索引（forward数组，node类型） 完完全全就是索引的概念的，之前对代码的理解存在问题了
    forward索引数组代表着每一层指向的下一个节点，实际的节点只有一个，就是第一层的原始节点，forward数组只存节点引用的话可以省去很多存储
    空间消耗
    """
    def __init__(self):
        self.data = -1
        # MAX_LEVEL理解成高度，索引层数是MAX_LEVEL-1
        self.forward = [None] * MAX_LEVEL
        self.max_level = 1


class SkipList:
    # 哨兵头节点
    head = Node()
    # 跳表高度，索引层数比此值小1，查找和删除时
    level_count = 1

    # 插入函数
    def insert(self, value):
        new_level = self.rand_level()
        p = self.head
        # 从当前跳表的最高层索引处开始查找合适的插入点进行插入操作
        search_level = max(self.level_count, new_level)
        new_node = Node()
        new_node.data = value
        new_node.max_level = search_level
        # 记录查找过程中插入节点在各级索引应该插入的位置
        update = self.get_update(search_level, p, value)

        # 更新索引，只需要更新new_level-1层的索引，第0级是实际的节点插入操作，后面new_level-1层是真正的索引层
        for i in range(new_level):
            new_node.forward[i] = update[i].forward[i]
            update[i].forward[i] = new_node

        # 更新跳表高度，当前跳表高度和新插入节点高度中的比较大值
        self.level_count = search_level

    def find(self, value):
        p = self.head
        # 找到最接近value点，最后根据是否为None和值是否为value来判断要找的点是否在跳表中
        for i in range(self.level_count-1, -1, -1):
            while p.forward[i] is not None and p.forward[i].data < value:
                p = p.forward[i]

        # 最后一层比较是因为有可能寻找的节点没有索引
        if p.forward[0] is not None and p.forward[0].data == value:
            return p.forward[0]
        else:
            return None

    def delete(self, value):
        level = self.level_count
        update = self.get_update(level, self.head, value)
        # 最下层的前序节点
        p = update[0]
        if p.forward[0] is not None and p.forward[0].data == value:
            for i in range(0, level):
                if update[i].forward[i] is not None and update[i].forward[i].data == value:
                    update[i].forward[i] = update[i].forward[i].forward[i]

        # 更新各个跳表层数
        while self.level_count > 1 and self.head.forward[self.level_count-1] is None:
            self.level_count -= 1

    @staticmethod
    def get_update(level, p, value):
        """
        获取值为value的节点的各个层级的前序节点
        """
        update = [None]*level
        for i in range(level - 1, -1, -1):
            # 这里实在太巧妙了，从i层找到p，然后再从p的i-1层开始往下找
            # 退出条件中forward为None时，i层开始的往下每一层索引都加在p之后
            while p.forward[i] is not None and p.forward[i].data < value:
                p = p.forward[i]
            update[i] = p
        return update

    @staticmethod
    def rand_level():
        # 生成node的随机高度，决定添加多少级的索引
        level = 1

        while level < MAX_LEVEL and random.random() < INDEX_P:
            level += 1

        return level


if __name__ == '__main__':
    sk = SkipList()
    for i in range(10):
        sk.insert(i)
    print(sk.find(7))
    sk.delete(7)
    print(sk.level_count)
    print(sk.find(7))