def longest_length_substring(s):
    char_set = set()
    n = len(s)
    i, j, max_length = 0, 0, 0

    while i < n and j < n:
        if s[j] in char_set:
            char_set.remove(s[i])
            i += 1
            continue
        max_length = max(max_length, j - i + 1)
        char_set.add(s[j])
        j += 1

    return max_length


def longest_length_substring_map(s):
    char_map = {}
    n = len(s)
    i, j, max_length = 0, 0, 0

    while j < n:
        if s[j] in char_map:
            i = max(i, char_map[s[j]] + 1)
        max_length = max(max_length, j - i + 1)
        char_map[s[j]] = j
        j += 1

    return max_length


if __name__ == "__main__":
    s = "abccefg"
    # print(longest_length_substring(s))
    print(longest_length_substring_map(s))
