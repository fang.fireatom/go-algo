#小孩分糖果

#字典表示小孩和小孩对糖果的需求
kid_candy = {'lily': 5, 'lucy': 3, 'noah': 1, 'emma': 4, 'liam': 2, 'mason': 3}
#糖果尺寸列表
candy_size = [10, 2, 3, 1]

def greedy_candy_distribution(kid_candy, candy_size):
    candy_size.sort()
    #字典是无序的，可以转换成元祖列表：[('noah', 1),('liam', 2)]
    kid_candy_list = sorted(kid_candy.items(), key = lambda item:item[1])
    satisfied_nums = 0
    for i in kid_candy_list:
        size = i[1]
        if len(candy_size) != 0 and size <= candy_size[0]:
            satisfied_nums += 1
            candy_size = candy_size[1:]
        else:
            break
    return satisfied_nums

#电台覆盖问题

#需要覆盖的州，集合表示
state_needed = set(['wa', 'mt', 'id', 'or', 'ca', 'az', 'nv', 'ut'])
#电台及其可以覆盖的州关系，散列表表示
stations = {}
stations['kone'] = set(['id', 'nv', 'ut'])
stations['ktwo'] = set(['wa', 'id', 'mt'])
stations['kthree'] = set(['or', 'ca', 'nv'])
stations['kfour'] = set(['nv', 'ut'])
stations['kfive'] = set(['ca', 'az'])

def which_stations(state_needed, stations):
    station_selected = set()
    while state_needed:
        station_best = None
        state_new_covered = set()
        for station, station_state in stations.items():
            #集合交集是能够新增的覆盖州
            covered = state_needed & station_state
            if len(covered) > len(state_new_covered):
                state_new_covered = covered
                station_best = station
        station_selected.add(station_best)
        state_needed -= state_new_covered
        #已经选择的电台从备选电台中删除
        del stations[station_best]
    return station_selected

if __name__ == '__main__':
    print(greedy_candy_distribution(kid_candy, candy_size))
    print(which_stations(state_needed, stations))
