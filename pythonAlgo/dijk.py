#设置无穷大常量
INFINITY = float('inf')
#图的数据结构：两层散列表，对应python的字典概念
graph = {}
graph['a'] = {}
graph['b'] = {}
graph['c'] = {}
graph['d'] = {}
graph['e'] = {}
graph['f'] = {}
graph['a']['b'] = 5
graph['a']['c'] = 0
graph['b']['d'] = 15
graph['b']['e'] = 20
graph['c']['d'] = 30
graph['c']['e'] = 35
graph['d']['f'] = 20
graph['e']['f'] = 10

#到达某个点的开销，使用散列表存储，字典。以a为起点初始化一下cost，这里直接人工初始化了，也即设置起点能直接到达的点
cost = {}
cost['b'] = 5
cost['c'] = 0
cost['d'] = INFINITY
cost['e'] = INFINITY
cost['f'] = INFINITY

#路径记录使用散列表结构，记录所到节点的前置节点,起始点开始的部分一样人工初始化
pre_node = {}
pre_node['b'] = 'a'
pre_node['c'] = 'a'
pre_node['d'] = None
pre_node['e'] = None
pre_node['f'] = None

#存储已经处理过的节点,使用数组结构，也即list
processed = []

#找到开销表中未处理节点中消耗最小的点
def find_lowest_cost_node(cost, processed):
    lowest_cost = INFINITY
    node = None
    for key in cost:
        if key not in processed and cost[key] < lowest_cost:
            lowest_cost = cost[key]
            node = key
    return node

#dijk算法实现
def dijk(graph, cost, processed, pre_node):
    node = find_lowest_cost_node(cost, processed)
    while node != None:
        next_nodes = graph[node]
        for key in next_nodes.keys():
            new_cost = cost[node] + next_nodes[key]
            if new_cost < cost[key]:
                cost[key] = new_cost
                pre_node[key] = node
        processed.append(node)
        node = find_lowest_cost_node(cost, processed)

#递归打印路径
def print_route(pre_node, s, t):
    if s != t:
        print_route(pre_node, s, pre_node[t])
    print(t)

if __name__ == "__main__":
    dijk(graph, cost, processed, pre_node)
    print_route(pre_node, 'a', 'f')
    print(cost['f'])