class Heap:
    #堆容量
    capacity = 0
    #已经存储的数据个数
    count = 0
    #存放堆数据的数组
    arr = []

    #构造方法方便创建类的时候直接传入参数，没有构造方方法也可以类创建成功后再赋值
    def __init__(self, capacity):
        self.capacity = capacity
        self.count = 0
        #列表第一个位置不存放数据,初始化空列表
        self.arr = [None]*(capacity+1)

    def insert(self, data):
        if self.count >= self.capacity:
            return
        self.count += 1
        self.arr[self.count] = data

        #循环将数据插入到正确的位置，向上比较
        i = self.count
        while i > 1 and self.arr[i] > self.arr[int(i/2)]:
            self.arr[i], self.arr[int(i/2)] = self.arr[int(i/2)], self.arr[i]
            i = int(i/2)

    def remove_max(self):
        if self.count <= 0:
            return -1
        self.arr[1] = self.arr[self.count]
        #这个保证了只有1个元素时，删除操作也适用
        self.count -= 1

        #堆化操作，这里独立成一个函数，后面堆排序的时候用的到
        self._heapfiy(self.arr, self.count, 1)

    def build_heap(self, arr, capacity):
        i = int(capacity/2)
        while i >= 1:
            self._heapfiy(arr, capacity, i)
            i -= 1

    def heap_sort(self, arr, capacity):
        self.build_heap(arr, capacity)
        k = capacity
        while k > 1:
            arr[k], arr[1] = arr[1], arr[k]
            k -= 1
            self._heapfiy(arr, k, 1)

    def _heapfiy(self, arr, count, i):
        while True:
            heap_max = i
            #注意这里的逻辑：最大值肯定在节点的左右子节点中产生
            if 2*i <= count and arr[i] < arr[2*i]:
                heap_max = 2*i
            if 2*i+1 <= count and arr[heap_max] < arr[2*i+1]:
                heap_max = 2*i+1
            if heap_max == i:
                break
            arr[i], arr[heap_max] = arr[heap_max], arr[i]
            i = heap_max





if __name__ == '__main__':
    heap = Heap(20)
    for k in range(10):
        heap.insert(k)
    heap.insert(11)
    for k in range(1, heap.count+1):
        print(heap.arr[k])
    print("\n")
    heap.remove_max()
    for k in range(1, heap.count+1):
        print(heap.arr[k])
    arr = list(range(10))
    print(arr)
    #数据从1开始存储
    heap.build_heap(arr, len(arr)-1)
    for i in range(1, len(arr)):
        print(arr[i])
    heap.heap_sort(arr, len(arr)-1)
    for i in range(1, len(arr)):
        print(arr[i])
