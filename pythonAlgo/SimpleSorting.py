#bubble sort
def bubble_sorting(arr):
    l = len(arr)
    flag = False
    for i in range(0, l - 1):
        for j in range(0, l - 1 - i):
            if arr[j] > arr[j+1]:
                arr[j], arr[j+1] = arr[j+1], arr[j]
                flag = True
        if not flag:
            break

#insertion sort
def insertion_sorting(arr):
    l = len(arr)
    for i in range(1, l):
        value = arr[i]
        j = i - 1
        while j >= 0:
            if value < arr[j]:
                arr[j+1] = arr[j]
            else:
                break
            j -= 1
        arr[j+1] = value

#selection sort
def selection_sorting(arr):
    l = len(arr)
    for i in range(l - 1):
        min = i
        for j in range(i+1, l):
            if arr[min] > arr[j]:
                min = j
        arr[i], arr[min] = arr[min], arr[i]

if __name__ == '__main__':
    arr = [5,4,33,3,2,1]
    # bubble_sorting(arr)
    # insertion_sorting(arr)
    selection_sorting(arr)
    print(arr)