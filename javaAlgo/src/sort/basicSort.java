package sort;

import java.util.Arrays;

public class basicSort {
    public static void bubbleSort(int[] a, int n) {
        //往后冒泡，每排一次，后面某一位完成排序
        if (n <= 1 ) return;
        for (int i = 0; i < n - 1; i++) {
            //是否发生交换的标志
            boolean flag = false;
            for (int j = 0; j < n -1 - i; j++) {
                if (a[j] > a[j+1]) {
                    int tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                    flag = true;
                }
            }
            //i主导的一次循环中，没有交换，说明已经排序完成可以提前退出
            if (!flag) break;
        }
    }

    //每次有交换的时候记录下最后一次交换的位置，这个就是下一次循环遍历冒泡的边界位置
    public static void bubbleSort2(int[] a, int n) {
        if (n <= 1) return;
        int sortBorder = n - 1;
        int lastExchange = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < sortBorder; j++) {
                if (a[j] > a[j+1]) {
                    int tmp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = tmp;
                    lastExchange = j;
                }
            }
            //不要在循环里修改循环边界条件变量，故此使用另一个变量传递值
            sortBorder = lastExchange;
        }
    }

    public static void insertionSort(int[] a, int n) {
        if (n <= 1) return;
        //把i插入前面的已排序数组[0,i-1]
        for (int i = 1; i < n; i++) {
            int value = a[i];
            //这里的起始也要想清楚
            int j = i - 1;
            for (; j >= 0; j--) {
                if (a[j] > value) {
                    //已排序数组中查找插入位置，出现顺序不对的就移位
                    a[j+1] = a[j];
                } else {
                    break;
                }
            }
            //用j来记录插入的正确位置，之前的是数组变量的移位
            a[j+1] = value;
        }
    }

    public static void selectionSort(int[] a, int n) {
        if (n <= 1) return;
        for (int i = 0; i < n - 1; i++) {
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (a[j] < a[min]) {
                    min = j;
                }
            }
            if (i != min) {
                int tmp = a[i];
                a[i] = a[min];
                a[min] = tmp;
            }
        }
    }

    public static void main(String[] args) {
        int[] array = new int[]{7,6,5,7,4,3,2,1};
        bubbleSort(array, array.length);
        System.out.println(Arrays.toString(array));
        array = new int[]{7,6,5,7,4,3,2,1};
        insertionSort(array, array.length);
        System.out.println(Arrays.toString(array));
        array = new int[]{7,6,5,7,4,3,2,1};
        selectionSort(array, array.length);
        System.out.println(Arrays.toString(array));
        array = new int[]{7,6,5,7,4,3,2,1};
        bubbleSort2(array, array.length);
        System.out.println(Arrays.toString(array));
    }
}
